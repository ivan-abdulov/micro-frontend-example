import React from 'react';

import { Button } from '@Components/Button';

const App: React.FC = () => (
  <div className="m-3">
    <Button>Hello Friends 🚀</Button>
  </div>
);

export default App;
