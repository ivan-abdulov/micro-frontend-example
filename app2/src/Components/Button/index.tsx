import React, { ReactNode, useCallback } from 'react';

interface Props {
  children?: ReactNode;
  className?: string;
  'data-testid'?: string;
}

const Button: React.FC<Props> = ({ children, className, 'data-testid': dataTestId }) => {
  const handleClick = useCallback(() => {
    // eslint-disable-next-line
        alert('click');
  }, []);
  return (
    <button type="button" data-testid={dataTestId} className={className} onClick={handleClick}>
      {children || <>Hello Friends 🚀</>}
    </button>
  );
};

export { Button };
