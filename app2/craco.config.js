const CracoAlias = require("craco-alias");
module.exports = {
    plugins: [
        {
            plugin: CracoAlias,
            options: {
                source: "tsconfig",
                tsConfigPath: "./tsconfig.paths.json",
                baseUrl: "./",
                // debug: true
            }
        },
        {
            plugin: require("./craco-plugins/module-federation")
        }
    ],
};
