# Getting Started with Create React App and rescripts.js

## For run project write this command

 - yarn
 - cd server && yarn 
 - cd ../ && yarn start

## For add new micro frontend module:

- add a new field to modules.js
- add a new script with src attribute to public/index.html
- add module declaration to src/declarations 
- add component with lazy import by example to src/modules/index.tsx
- use a new module as typical react component

## For expose current app 

- change .env file and write a new HOST and PORT
- change name in package.json

## For add new alias:
 
- add a new field to alias.json by a path compilerOptions.paths
