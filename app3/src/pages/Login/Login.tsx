import React, { useEffect, useMemo } from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Redirect, RouteComponentProps } from 'react-router-dom';

import { getLoginParams } from '@app/configs/utils';
import { IGetTokenRequestThunkParams } from '@app/store/login/interfaces';
import { LoginTypes } from '@app/utils/api/login/constants';
import { Routes } from '@app/utils/routes/routesMap';
import Loader from '@components/Loader/Loader';
import LoginForm from '@components/LoginForm/LoginForm';
import { getTokenRequestThunk } from '@store/login/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

const Login: React.FC<RouteComponentProps> = ({ match: { path } }) => {
  const dispatch = useLocalDispatch();
  const { isAuthenticated, loginLoading, form } = useLocalSelector((state) => state.login);
  const loginType = useMemo(() => (path === Routes.loginLocal ? LoginTypes.loginLocal : LoginTypes.login), [path]);
  const loginParams = useMemo(() => getLoginParams(loginType), [loginType]);

  useEffect(() => {
    let loginTypeStorred;
    switch (path) {
      case Routes.loginLocal:
        loginTypeStorred = LoginTypes.loginLocal;
        break;
      case Routes.loginAuto:
        loginTypeStorred = LoginTypes.loginAuto;
        break;
      default:
        loginTypeStorred = LoginTypes.login;
    }
    sessionStorage.setItem('loginType', loginTypeStorred);
    const params: IGetTokenRequestThunkParams = {};
    if (Routes.login === path) {
      params.skipAutoLogin = true;
    }
    dispatch(getTokenRequestThunk(params));
  }, [dispatch, loginParams, loginType, path]);

  if (loginLoading && !form) {
    return <Loader />;
  }

  if (isAuthenticated) {
    return <Redirect to={'/'} />;
  }

  return <LoginForm loginParams={loginParams} />;
};

export default Login;
