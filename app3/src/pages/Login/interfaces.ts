export interface ILoginFormValues {
  password: string;
  username: string;
}
