import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import Loader from '@app/components/Loader/Loader';
import { ILogoutThunkParams } from '@app/store/login/interfaces';
import { logoutThunk } from '@store/login/slice';
import { useLocalDispatch } from '@utils/hooks/useRedux';

const Logout: React.FC<RouteComponentProps> = ({ location }) => {
  const dispatch = useLocalDispatch();

  useEffect(() => {
    dispatch(logoutThunk({ ...(location.state as ILogoutThunkParams) }));
  }, [dispatch, location]);

  return <Loader />;
};

export default Logout;
