export interface IHomeProps {
  search_platform: string;
  search_platform_value: string;
  search_email?: string;
  search_msisdn?: string;
}
