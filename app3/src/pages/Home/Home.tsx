import cn from 'classnames';
import React, { SyntheticEvent, useCallback, useEffect, useMemo } from 'react';
import { RouteComponentProps } from 'react-router';

import UserBlock from '@app/components/UserBlock/UserBlock';
import { useIsB2b } from '@app/utils/hooks/customHooks';
import { Routes } from '@app/utils/routes/routesMap';
import Loader from '@components/Loader/Loader';
import SearchForm from '@components/SearchForm/SearchForm';
import { IHomeProps } from '@pages/Home/interfaces';
import { clearMessages } from '@store/messages/slice';
import { loadUserData, setSearchParams } from '@store/user/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import styles from './Home.module.scss';

const Home = (props: RouteComponentProps<IHomeProps>): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const { isAuthenticated } = useLocalSelector((state) => state.login);
  const { list } = useLocalSelector((state) => state.user.userData);
  const isB2b = useIsB2b();

  const {
    userData: { error, isLoading },
  } = useLocalSelector((state) => state.user);

  const {
    match: {
      params: { search_msisdn, search_email, search_platform, search_platform_value },
      params,
    },
  } = props;

  const isSearcParamsValid = useMemo(
    () => search_msisdn || search_email || (search_platform && search_platform_value),
    [search_msisdn, search_email, search_platform, search_platform_value]
  );

  const onRegistration = useCallback(
    (e: SyntheticEvent): void => {
      e.preventDefault();
      props.history.push(Routes.registration);
    },
    [props.history]
  );

  useEffect((): void => {
    dispatch(clearMessages());
    if (isSearcParamsValid && isAuthenticated) {
      dispatch(setSearchParams(params));
      void dispatch(loadUserData({ ...params }));
    }
  }, [dispatch, isAuthenticated, isSearcParamsValid, params]);

  const getUserData = (): React.ReactElement | null => {
    if (error) {
      return (
        <div className="container empty-form-title">
          <br />
          <h4>{error}</h4>
        </div>
      );
    }

    return (
      <div>
        {Object.keys(list).map(
          (userId: string): React.ReactElement => (
            <div key={userId} className={cn(styles.userBlockWrapper, 'container')}>
              <UserBlock userId={userId} history={props.history} />
            </div>
          )
        )}
      </div>
    );
  };

  return (
    <div>
      {isAuthenticated && (
        <div className="container">
          <div className="row">
            <SearchForm history={props.history} {...params} />
            {isB2b && (
              <div className={styles.addUserButtonWrapper}>
                <button onClick={onRegistration}>Добавить нового пользователя</button>
              </div>
            )}
          </div>
        </div>
      )}
      {isLoading && <Loader />}

      {getUserData()}
    </div>
  );
};

export default Home;
