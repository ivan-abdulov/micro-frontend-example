import moment from 'moment';

import { TGetDictionaries } from '@utils/api/user/interfaces';

export const datetimeFormatter = (cellvalue: string): string => moment(cellvalue).format('DD.MM.yyyy HH:mm:ss');

export const whereFormatter = (dictionaries?: TGetDictionaries) => (cellvalue: string): string => {
  switch (cellvalue) {
    case 'mgf_lk':
    case 'lk_gf':
    case 'mlk':
    case 'mlk_pwd':
      return 'Личный Кабинет';

    case 'lk_ro':
    case 'b2b_site':
      return 'МегаФон Бизнес';

    case 'lk_8800':
      return '8800';

    case 'lk_kk':
    case 'lk_kk2':
      return 'Контроль кадров';

    case 'lk_vats':
      return 'ВАТС';

    case 'lk_target':
    case 'lk_target_m2m':
      return 'Таргет';

    case 'lk_multifon':
      return 'МультиФон';

    case 'lk_b2b':
      return 'ЛК B2B';

    case 'lk_vats_m2m':
      return 'ВАТС';
  }

  const hashDict = {};
  (dictionaries || []).forEach((item) => {
    hashDict[item.k] = item.v;
  });

  let result = hashDict[cellvalue];
  if (!result && cellvalue) {
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    result = cellvalue.substring(cellvalue.lastIndexOf('.') + 1);
  }
  return result || 'Unknown';
};

export const eventTypeFormatter = (
  cellvalue: string,
  rowObject: {
    data: {
      partnerId: string;
    };
    name: string;
  }
): string => {
  // eslint-disable-next-line sonarjs/no-duplicate-string
  if (cellvalue === 'autologin' || rowObject.name === 'sso.auth.auto.fail') {
    return 'Автовход';
  }
  if (rowObject.name === 'sso.auth.otp_code.success' || rowObject.name === 'sso.auth.otp_code.fail') {
    return 'Ввод OTP';
  }
  // eslint-disable-next-line sonarjs/no-duplicate-string
  if (rowObject.name === 'sso.auth.success' || rowObject.name === 'sso.auth.fail') {
    if (rowObject.data && rowObject.data.partnerId) {
      return 'Вход через внешний сервис';
    }
    return 'Вход по логину/паролю';
  }
  return cellvalue || '???';
};

export const apiFormatter = (cellvalue: string): string => {
  if (cellvalue === 'oauth2') {
    return 'OAuth 2.0';
  }
  if (cellvalue === 'mlk') {
    return 'M2M';
  }
  return cellvalue;
};

export const nameFormatter = (
  cellvalue: string,
  rowObject: {
    error: string;
    errorSubtype: string;
  }
): string => {
  let result = '';

  if (
    cellvalue === 'sso.auth.fail' &&
    rowObject.error === 'invalid_grant' &&
    rowObject.errorSubtype === 'invalid_captcha'
  ) {
    result = 'Неуспешно<br>Ошибка в капче';
  } else if (
    cellvalue === 'sso.auth.fail' &&
    rowObject.error === 'invalid_grant' &&
    rowObject.errorSubtype === 'invalid_credentials'
  ) {
    result = 'Неуспешно<br>Ошибка в логине/пароле';
  } else if (
    cellvalue === 'sso.auth.fail' &&
    rowObject.error === 'invalid_grant' &&
    rowObject.errorSubtype === 'system_blocked'
  ) {
    result = 'Неуспешно<br>Система заблокирована';
  } else if (
    cellvalue === 'sso.auth.fail' &&
    rowObject.error === 'invalid_grant' &&
    rowObject.errorSubtype === 'user_blocked'
  ) {
    result = 'Неуспешно<br>Пользователь заблокирован';
  } else if (
    cellvalue === 'sso.auth.fail' &&
    rowObject.error === 'invalid_client' &&
    rowObject.errorSubtype === 'user_blocked'
  ) {
    result = 'Неуспешно<br>client_id заблокирован';
  } else if (
    cellvalue === 'sso.auth.success' ||
    cellvalue === 'sso.auth.autologin.token.created' ||
    cellvalue === 'sso.auth.otp_code.success'
  ) {
    result = 'Успешно';
  } else if (cellvalue === 'sso.auth.otp_code.fail' || cellvalue === 'sso.auth.auto.fail') {
    result = 'Неуспешно';
  } // end if
  return result;
};

export const detailsFormatter = (
  cellvalue: string,
  rowObject: {
    data: {
      fullName: string;
      partnerId: string;
    };
    name: string;
  }
): string => {
  if (
    (rowObject.name === 'sso.auth.success' || rowObject.name === 'sso.auth.fail') &&
    rowObject.data &&
    rowObject.data.partnerId
  ) {
    if (rowObject.data.fullName) {
      return rowObject.data.partnerId + ' ' + rowObject.data.fullName;
    }
    return rowObject.data.partnerId;
  }
  return '';
};

export const geoFormatter = (
  cellvalue: string,
  rowObject: {
    geoIPCityNameNat: string;
    geoIPCountry: string;
    geoIPRegionNameNat: string;
  }
): string =>
  (rowObject.geoIPCountry || 'Unknown') +
  ', ' +
  (rowObject.geoIPRegionNameNat || 'Unknown') +
  ', ' +
  (rowObject.geoIPCityNameNat || 'Unknown');

export const browserFormatter = (
  cellvalue: string,
  rowObject: {
    userAgent: string;
    userAgentBrowserFamily: string;
    userAgentBrowserNameVersion: string;
  }
): string => {
  if (
    rowObject.userAgentBrowserNameVersion &&
    rowObject.userAgentBrowserFamily &&
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
    rowObject.userAgentBrowserFamily.indexOf('Unknown') === -1
  ) {
    return rowObject.userAgentBrowserNameVersion || 'Unknown';
  }
  return rowObject.userAgent || 'Unknown';
};

export const ipFormatter = (cellvalue: string, rowObject: { networkType: number }): string => {
  // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  if (typeof rowObject.networkType !== 'undefined' && rowObject.networkType === 1) {
    return cellvalue + ' - сеть МегаФон';
  }

  return cellvalue + ' - внешний интернет';
};
