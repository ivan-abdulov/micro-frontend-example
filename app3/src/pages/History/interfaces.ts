export interface IHistoryProps {
  page: string;
  search_end: string;
  search_start: string;
  time: string;
  search_email?: string;
  search_msisdn?: string;
}
