import cn from 'classnames';
import React, { useCallback, useEffect, useMemo } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link, RouteComponentProps } from 'react-router-dom';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { makeSearchUrl } from '@app/utils/api/user/utils';
import HistoryFilterForm from '@components/HistoryFilterForm/HistoryFilterForm';
import Loader from '@components/Loader/Loader';
import { getOperationAudit } from '@store/user/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import styles from './History.module.scss';
import { IHistoryProps } from './interfaces';
import {
  apiFormatter,
  browserFormatter,
  datetimeFormatter,
  detailsFormatter,
  eventTypeFormatter,
  geoFormatter,
  ipFormatter,
  nameFormatter,
  whereFormatter,
} from './utils';

const History = (props: RouteComponentProps<IHistoryProps>): React.ReactElement => {
  const {
    match: {
      params: { search_msisdn, search_start, search_end, page },
    },
    history,
  } = props;
  const dispatch = useLocalDispatch();
  const {
    error,
    operationAudit,
    dictionaries,
    operationAuditPageSize,
    operationAuditPageNumber,
    operationAuditTotalElements,
    isLoading,
  } = useLocalSelector((state) => state.user.operationAuditData);
  const { search_email, search_platform, search_platform_value } = useLocalSelector((state) => state.user.userData);
  const { isAuthenticated } = useLocalSelector((state) => state.login);

  useEffect(() => {
    if (search_msisdn && isAuthenticated) {
      void dispatch(
        getOperationAudit({
          search_msisdn,
          search_start,
          search_end,
          page: parseInt(page, 10),
        })
      );
    }
  }, [dispatch, page, search_end, search_msisdn, search_start, isAuthenticated]);

  const onPageChange = useCallback(
    (nextPage: number): void => {
      history.push(`/history/${search_msisdn}/${search_start}/${search_end}/${nextPage}`);
    },
    [history, search_end, search_msisdn, search_start]
  );
  const backUrl = useMemo(
    () => makeSearchUrl({ search_email, search_platform, search_platform_value, search_msisdn: search_msisdn || '' }),
    [search_email, search_msisdn, search_platform, search_platform_value]
  );

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className={cn('col-md-12', styles.containerBack)}>
            <Link to={backUrl} className="btn btn-default">
              <>
                <i className="fa fa-chevron-left t" aria-hidden="true" />
                &nbsp; Назад
              </>
            </Link>
          </div>
          <div className="col-md-12">
            <h4>ИСТОРИЯ ОПЕРАЦИЙ для {search_msisdn}</h4>
          </div>
          <div className="col-md-12">
            <HistoryFilterForm
              history={history}
              search_end={search_end}
              search_msisdn={search_msisdn}
              search_start={search_start}
            />
          </div>
          {error && (
            <div className={cn(styles.alertDangerOpAudit, 'col-md-12')}>
              <div className="alert alert-danger" role="alert">
                {error}
              </div>
            </div>
          )}
        </div>
      </div>
      <div className={styles.containerBtable}>
        {isLoading && <Loader />}
        {operationAudit && (
          <BootstrapTable
            data={operationAudit}
            remote
            pagination
            striped
            hover
            options={{
              sizePerPage: operationAuditPageSize,
              // eslint-disable-next-line @typescript-eslint/no-magic-numbers
              page: operationAuditPageNumber || 1,
              hideSizePerPage: true,
              onPageChange,
            }}
            fetchInfo={{ dataTotalSize: operationAuditTotalElements || 0 }}>
            <TableHeaderColumn isKey dataField="authType" dataFormat={eventTypeFormatter} width="194">
              Тип операции
            </TableHeaderColumn>
            <TableHeaderColumn dataField="timeStart" dataFormat={datetimeFormatter} width="150">
              Время операции
            </TableHeaderColumn>
            <TableHeaderColumn dataField="authEndpointType" dataFormat={apiFormatter} width="90">
              API
            </TableHeaderColumn>
            <TableHeaderColumn dataField="name" dataFormat={nameFormatter} width="194">
              Результат
            </TableHeaderColumn>

            <TableHeaderColumn dataField="clientId" dataFormat={whereFormatter(dictionaries)} width="194">
              Вход куда
            </TableHeaderColumn>

            <TableHeaderColumn dataField="correlationId" width="160">
              Correlation ID
            </TableHeaderColumn>

            <TableHeaderColumn dataField="details" dataFormat={detailsFormatter} width="120">
              Подробности
            </TableHeaderColumn>

            <TableHeaderColumn dataField="geoIPCityId" dataFormat={geoFormatter} width="220">
              Местоположение
            </TableHeaderColumn>
            <TableHeaderColumn dataField="ipAddressString" dataFormat={ipFormatter} width="240">
              IP-адрес
            </TableHeaderColumn>
            <TableHeaderColumn dataField="imeiDeviceModel" width="140">
              Устройство (IMEI)
            </TableHeaderColumn>
            <TableHeaderColumn dataField="userAgentDeviceModel" width="194">
              Устройство (UA)
            </TableHeaderColumn>

            <TableHeaderColumn dataField="userAgentOSNameVersion" width="194">
              Операционная система
            </TableHeaderColumn>
            <TableHeaderColumn dataField="userAgentBrowserNameVersion" dataFormat={browserFormatter} width="120">
              Браузер
            </TableHeaderColumn>

            <TableHeaderColumn dataField="autologin" width="90">
              Автовход
            </TableHeaderColumn>
          </BootstrapTable>
        )}
      </div>
    </div>
  );
};

export default History;
