// This import syntax is required for use with Webpack 5 Module Federation when we use exposes module
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
void import('./App');
