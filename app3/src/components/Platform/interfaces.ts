import { IPlatform, Ival } from '@store/user/interfaces';

export interface IPlatformProps {
  handleRemove: (platform: Ival) => () => void;
  mappingsList: Ival[];
  platform: IPlatform;
  userId?: string;
}
