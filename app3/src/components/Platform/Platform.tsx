import React, { useCallback, useState } from 'react';

import AddPlatformForm from '@components/AddPlatformForm/AddPlatformForm';
import PlatformMapping from '@components/PlatformMapping/PlatformMapping';
import { Ival } from '@store/user/interfaces';

import styles from './Platform.module.scss';
import { IPlatformProps } from './interfaces';

const Platform = ({ platform, handleRemove, mappingsList, userId }: IPlatformProps): React.ReactElement => {
  const [isFormShown, setIsFormShown] = useState(false);
  const handleToggleForm = useCallback(() => {
    setIsFormShown(!isFormShown);
  }, [setIsFormShown, isFormShown]);
  return (
    <tr key={platform.code}>
      <td>
        <div className={styles.platformHeader}>
          <i className="fa fa-address-card fa-2x pull-left" aria-hidden="true" />
          <h5>{platform.name}</h5>
          <div className={styles.addPlatformButtonWrapper}>
            {!isFormShown && (
              <button type="button" className="btn btn-success" onClick={handleToggleForm}>
                <i className="fa fa-plus" aria-hidden="true" />
              </button>
            )}
          </div>
        </div>
        {isFormShown && userId && (
          <AddPlatformForm onToggleForm={handleToggleForm} platform={platform} userId={userId} />
        )}

        {mappingsList &&
          mappingsList.map((item: Ival) => (
            <PlatformMapping
              key={`${String(item.partnerId)}_${item.phonenum}`}
              val={item}
              handleRemove={handleRemove}
            />
          ))}
      </td>
    </tr>
  );
};

export default Platform;
