import { VALIDATION_PATTERNS } from '@app/utils/constants';
import { phoneFormatter } from '@app/utils/formatters';

export const PATTERNS = {
  email: {
    pattern: VALIDATION_PATTERNS.email,
    params: {
      placeholder: 'нaпр. test@test.com',
    },
    formatter: (value: string): string => value,
  },
  phone: {
    isCustomComponent: true,
    pattern: VALIDATION_PATTERNS.phone,
    params: {
      placeholder: 'напр. +7 (921) 111-05-00',
      mask: '+7 (999) 999-99-99',
    },
    formatter: (value: string): string => phoneFormatter(value, false),
  },
};
