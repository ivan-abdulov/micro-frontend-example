import React, { useCallback, useMemo } from 'react';
import { Controller, useForm } from 'react-hook-form';
import InputMask from 'react-input-mask';

import { clearMessages, showError, showSuccess } from '@app/store/messages/slice';
import { addPlatformLink } from '@app/store/user/slice';
import { WRONG_PARAMS } from '@app/utils/api/user/constants';
import { useLocalDispatch, useLocalSelector } from '@app/utils/hooks/useRedux';
import { PATTERNS } from '@components/AddPlatformForm/constants';
import { unwrapResult } from '@reduxjs/toolkit';

import styles from './AddPlatformForm.module.scss';
import { IAddPlatformForm, IAddPlatformFormProps } from './interfaces';

const AddPlatformForm = ({ onToggleForm, platform, userId }: IAddPlatformFormProps): React.ReactElement => {
  const userData = useLocalSelector((state) => (userId ? state.user.userData.list[userId] : {}));
  const dispatch = useLocalDispatch();
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    control,
  } = useForm<IAddPlatformForm>({
    defaultValues: {
      value: '',
    },
    mode: 'onChange',
  });

  const { pattern, params, isCustomComponent, formatter } = useMemo(() => PATTERNS[platform.type], [platform.type]);
  const inputParams = useMemo(
    () => ({
      ...params,
      className: errors.value ? 'form-control js-msisdn  input-sf alert-f' : 'form-control js-msisdn  input-sf',
      name: 'value',
      ref: register({
        required: true,
        pattern,
      }),
    }),
    [params, errors.value, register, pattern]
  );

  const onAddPlatformFormSubmit = useCallback(
    async ({ value }: IAddPlatformForm): Promise<void> => {
      if (!userData.msisdn || !userId) {
        dispatch(showError({ msg: WRONG_PARAMS }));
        return;
      }
      try {
        dispatch(clearMessages());
        const result = await dispatch(
          addPlatformLink({
            userId,
            params: {
              customerId: userData.msisdn,
              partnerId: platform.code,
              externalUserId: formatter(value),
            },
          })
        );
        unwrapResult(result);
        dispatch(showSuccess({ msg: 'Операция выполнена успешно!' }));
        onToggleForm();
      } catch (e) {
        dispatch(showError({ msg: e.message }));
      }
    },
    [userData.msisdn, userId, dispatch, platform.code, formatter, onToggleForm]
  );
  return (
    <div className={styles.addPlatformForm}>
      <form onSubmit={handleSubmit(onAddPlatformFormSubmit)}>
        {isCustomComponent ? (
          <Controller
            name="value"
            control={control}
            rules={{
              required: true,
              pattern,
            }}
            render={(field, { invalid }) => (
              <InputMask
                className={invalid ? 'form-control js-mask alert-f' : 'form-control js-mask'}
                id="value"
                {...params}
                {...field}
              />
            )}
          />
        ) : (
          <input {...inputParams} />
        )}

        <button type="submit" className="btn btn-success" disabled={!isValid}>
          <i className="fa fa-check" aria-hidden="true" />
        </button>
        <button type="button" className="btn btn-danger" onClick={onToggleForm}>
          <i className="fa fa-times" aria-hidden="true" />
        </button>
      </form>
    </div>
  );
};

export default AddPlatformForm;
