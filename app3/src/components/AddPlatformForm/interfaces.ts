import { IPlatform } from '@app/store/user/interfaces';

export interface IAddPlatformForm {
  value: string;
}
export interface IAddPlatformFormProps {
  onToggleForm: () => void;
  platform: IPlatform;
  userId?: string;
}
