import ru from 'date-fns/locale/ru';
import moment from 'moment';
import React, { useCallback } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import 'moment/locale/ru';
import { Controller, useForm } from 'react-hook-form';

import { NO_MSISDN_ERROR_MESSAGE } from '@app/store/messages/constants';
import { getOperationAudit } from '@app/store/user/slice';
import { DEFAULT_DATE_INTERVAL, MSISDN_LENGTH, URI_DATE_FORMAT } from '@app/utils/constants';
import { formatURLForHistory } from '@app/utils/formatters';
import { clearMessages, showError } from '@store/messages/slice';
import { useLocalDispatch } from '@utils/hooks/useRedux';

import styles from './HistoryFilterForm.module.scss';
import { ISearchFormProps } from './interfaces';

moment.locale('ru');

interface ISearchFormValues {
  search_end: Date;
  search_msisdn: string;
  search_start: Date;
}

const HistoryFilterForm = ({
  history,
  search_msisdn,
  search_start,
  search_end,
}: ISearchFormProps): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const {
    control,
    watch,
    formState: { isValid },
    handleSubmit,
  } = useForm<ISearchFormValues>({
    defaultValues: {
      search_start: search_start
        ? moment(search_start, URI_DATE_FORMAT).toDate()
        : moment().subtract(DEFAULT_DATE_INTERVAL, 'd').toDate(),
      search_end: search_end ? moment(search_end, URI_DATE_FORMAT).toDate() : new Date(),
    },
    mode: 'onChange',
  });

  const onSearchFormSubmit = useCallback(
    (values: ISearchFormValues): void => {
      if (!search_msisdn) {
        dispatch(showError({ msg: NO_MSISDN_ERROR_MESSAGE }));
        return;
      }
      const url = formatURLForHistory(search_msisdn, values.search_start, values.search_end);
      dispatch(clearMessages());
      if (history.location.pathname === url) {
        void dispatch(
          getOperationAudit({
            search_msisdn: search_msisdn.substr(-MSISDN_LENGTH),
            search_start: moment(values.search_start).utc().format(URI_DATE_FORMAT),
            search_end: moment(values.search_end).utc().format(URI_DATE_FORMAT),
            page: 1,
          })
        );
      } else {
        history.push(url);
      }
    },
    [dispatch, history, search_msisdn]
  );

  return (
    <div>
      <div className="row">
        <div className="col-md-12">
          <div className={styles.filterForm}>
            <form className="form-inline" onSubmit={handleSubmit(onSearchFormSubmit)}>
              <div className="form-group">
                <b>Дата/время начала</b>&nbsp;
                <label className="input-group date" id="datetimepicker_start">
                  <Controller
                    name="search_start"
                    control={control}
                    rules={{
                      required: true,
                      validate: (value) => value.getTime() < watch('search_end').getTime(),
                    }}
                    render={(field, { invalid }) => (
                      <DatePicker
                        className={
                          invalid ? 'form-control js-start input-sf alert-f' : 'form-control js-start input-sf'
                        }
                        selected={field.value || null}
                        locale={ru}
                        dateFormat="dd.MM.yyyy HH:mm"
                        {...field}
                        showTimeSelect
                      />
                    )}
                  />
                  <span className="input-group-addon">
                    <i className="fa fa-calendar" aria-hidden="true" />
                  </span>
                </label>
              </div>
              <div className="form-group">
                <b>Дата/время окончания</b>&nbsp;
                <label className="input-group date" id="datetimepicker_end">
                  <Controller
                    name="search_end"
                    control={control}
                    rules={{
                      required: true,
                      validate: (value) => value.getTime() > watch('search_start').getTime(),
                    }}
                    render={(field, { invalid }) => (
                      <DatePicker
                        className={invalid ? 'form-control input-sf  alert-f' : 'form-control input-sf'}
                        selected={field.value || null}
                        locale={ru}
                        dateFormat="dd.MM.yyyy HH:mm"
                        showTimeSelect
                        {...field}
                      />
                    )}
                  />
                  <div className="input-group-addon">
                    <i className="fa fa-calendar" aria-hidden="true" />
                  </div>
                </label>
              </div>
              <button className="btn btn-primary js-history" type="submit" disabled={!isValid}>
                <i className="fa fa-search" aria-hidden="true" />
                &nbsp; Найти
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HistoryFilterForm;
