import * as H from 'history';

export interface ISearchFormProps {
  history: H.History;
  search_end: string;
  search_start: string;
  search_email?: string;
  search_msisdn?: string;
}
