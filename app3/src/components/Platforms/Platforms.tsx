import React, { useCallback, useState } from 'react';

import { unwrapResult } from '@reduxjs/toolkit';

import { TRootState } from '@app/store';
import { platforms } from '@app/store/user/constants';
import { WRONG_PARAMS } from '@app/utils/api/user/constants';
import ConfirmModal from '@components/ConfirmModal/ConfirmModal';
import Platform from '@components/Platform/Platform';
import { clearMessages, showError, showSuccess } from '@store/messages/slice';
import { IPlatform, Ival } from '@store/user/interfaces';
import { removePlatformLink } from '@store/user/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import styles from './Platforms.module.scss';
import { IConfirmParams, IPlatformsProps } from './interfaces';

const Platforms = ({ userId }: IPlatformsProps): React.ReactElement => {
  const [confirmParams, setConfirmParams] = useState<IConfirmParams>({
    showConfirm: false,
    confirmTitle: '',
    confirmCallback: null,
  });
  const handleCloseConfirm = useCallback(() => {
    setConfirmParams({
      showConfirm: false,
      confirmTitle: '',
      confirmCallback: null,
    });
  }, [setConfirmParams]);

  const dispatch = useLocalDispatch();
  const { partnerMappings } = useLocalSelector((state: TRootState) => state.user.userData.list[userId]);
  const search_msisdn = useLocalSelector((state: TRootState) => state.user.userData.search_msisdn);

  const handleRemove = useCallback(
    (platformName: string) => (val: Ival) => () => {
      setConfirmParams({
        showConfirm: true,
        confirmTitle: `Вы уверены, что хотите удалить привязку ${search_msisdn} с ${val.externalUserId} в Продуктовой платформе "${platformName}"?`,
        confirmCallback: async (): Promise<void> => {
          try {
            if (!userId) {
              throw new Error(WRONG_PARAMS);
            }
            dispatch(clearMessages());
            unwrapResult(
              await dispatch(
                removePlatformLink({
                  userId,
                  val,
                })
              )
            );
            dispatch(showSuccess({ msg: 'Операция выполнена успешно!' }));
          } catch (error) {
            dispatch(showError({ msg: error.message || 'Ошибка соединения!' }));
          }
        },
      });
    },
    [dispatch, search_msisdn, userId]
  );

  const renderPlatform = useCallback(
    (platform: IPlatform): React.ReactElement => (
      <Platform
        key={platform.code}
        platform={platform}
        mappingsList={(partnerMappings || {})[platform.code]}
        handleRemove={handleRemove(platform.name)}
        userId={userId}
      />
    ),
    [partnerMappings, handleRemove, userId]
  );

  return (
    <div className="container">
      <ConfirmModal {...confirmParams} closeCallback={handleCloseConfirm} />
      <div className="row">
        <div className="col-md-5">
          <div className={styles.usersTable}>
            <table className="table table-striped table-bordered">
              <tbody>{platforms.left.map(renderPlatform)}</tbody>
            </table>
          </div>
        </div>

        <div className="col-md-5">
          <div className={styles.usersTable}>
            <table className="table table-striped table-bordered">
              <tbody>{platforms.right.map(renderPlatform)}</tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Platforms;
