export interface IConfirmParams {
  confirmCallback: (() => Promise<void> | void) | null;
  confirmTitle: string;
  showConfirm: boolean;
}

export interface IPlatformsProps {
  userId: string;
}
