import { scrollbarSize } from 'dom-helpers';
import React, { useEffect } from 'react';

const Loader = (): React.ReactElement => {
  useEffect(() => {
    const scrollWidth = scrollbarSize();
    const navbar = document.getElementById('mainNavbar');
    document.body.className = 'overflow-scroll';
    document.body.style.paddingRight = `${scrollWidth}px`;
    if (navbar) {
      navbar.style.marginRight = `-${scrollWidth}px`;
      navbar.style.paddingRight = `${scrollWidth}px`;
    }
    return () => {
      document.body.className = '';
      document.body.style.paddingRight = '';
      if (navbar) {
        navbar.style.marginRight = '';
        navbar.style.paddingRight = '';
      }
    };
  }, []);

  return (
    <div className="spinner-wrapper">
      <div className="spinner" />
    </div>
  );
};

export default Loader;
