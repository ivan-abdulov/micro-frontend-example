import React, { useCallback } from 'react';
import { Button, Modal } from 'react-bootstrap';

import { IConfirmProps } from './interfaces';

const ConfirmModal = (props: IConfirmProps): React.ReactElement => {
  let title: React.ReactElement | string | null = '';
  let text: React.ReactElement | string | null = '';

  if (typeof props.confirmTitle !== 'undefined' && props.confirmTitle !== '') {
    title = props.confirmTitle;
  }
  if (typeof props.confirmText !== 'undefined' && props.confirmText !== '') {
    text = props.confirmText;
  }

  if (title === '' && text === '') {
    title = 'Выполнить операцию?';
    text = 'Подтвердите действие.';
  } // end if

  const handleCloseConfirmModal = useCallback(() => {
    props.closeCallback();
  }, [props]);

  const handleSubmitConfirmModal = useCallback(() => {
    props.closeCallback();
    if (props.confirmCallback) {
      props.confirmCallback();
    }
  }, [props]);
  return (
    <div className="static-modal">
      <Modal show={props.showConfirm} onHide={handleCloseConfirmModal}>
        {text ? (
          <>
            <Modal.Header closeButton>
              <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>{text}</p>
            </Modal.Body>
          </>
        ) : (
          <Modal.Body>
            <h4>{title}</h4>
          </Modal.Body>
        )}

        <Modal.Footer>
          <Button onClick={handleCloseConfirmModal}>Отмена</Button>
          <Button bsStyle="primary" onClick={handleSubmitConfirmModal}>
            Да
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
export default ConfirmModal;
