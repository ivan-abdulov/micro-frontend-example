import React from 'react';

export interface IConfirmProps {
  closeCallback: () => void;
  confirmCallback: (() => void) | null;
  confirmTitle: string;
  showConfirm: boolean;
  confirmText?: React.ReactElement | string | null;
}
