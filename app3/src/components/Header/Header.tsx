import cn from 'classnames';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

import { Routes } from '@app/utils/routes/routesMap';
import { useLocalSelector } from '@utils/hooks/useRedux';

import styles from './Header.module.scss';
import { getTimeZone } from './utils';

const Header = (): React.ReactElement => {
  const { isAuthenticated, userName } = useLocalSelector((state) => state.login);

  return (
    <nav id="mainNavbar" className="navbar navbar-inverse bs-dark">
      <div className={cn('container', styles.container)}>
        <div className={cn('navbar-header', styles.navbarHeader)}>
          <Link to="/arm/contact-center/home" className="navbar-brand">
            <i className="fa fa-users" aria-hidden="true" />
            &nbsp; <FormattedMessage id="header.title" />
          </Link>
        </div>

        <div className={cn('navbar-right', styles.navbarRight)}>
          <div className={cn('navbar-right-block', styles.navbarRightBlock)}>
            <span className={cn('time', styles.time)}>
              <FormattedMessage id="header.timezone" />{' '}
              <span>
                <b>{getTimeZone()}</b>
              </span>
            </span>

            {isAuthenticated && (
              <>
                <span className={cn('top-username', styles.username)}>
                  <i className="fa fa-user" aria-hidden="true" />
                  &nbsp;
                  <span>{userName}</span>
                </span>
                <Link
                  to={{
                    pathname: Routes.logout,
                    state: {
                      manual: true,
                    },
                  }}>
                  <i className="fa fa-sign-out-alt fa-lg" />
                </Link>
              </>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
