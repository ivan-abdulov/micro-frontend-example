export const getTimeZone = (): string => {
  const curDateTime = new Date();
  const hoursDiffStdTime = -(curDateTime.getTimezoneOffset() / 60);
  let curTimeZone;
  // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  if (hoursDiffStdTime === 2) {
    curTimeZone = 'Калининградское время (MSK-1)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 3) {
    curTimeZone = 'Московское время (MSK)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 4) {
    curTimeZone = 'Самарское время (MSK+1)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 5) {
    curTimeZone = 'Екатеринбургское время (MSK+2)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 6) {
    curTimeZone = 'Омское время (MSK+3)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 7) {
    curTimeZone = 'Красноярское время (MSK+4)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 8) {
    curTimeZone = 'Иркутское время (MSK+5)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 9) {
    curTimeZone = 'Якутское время (MSK+6)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 10) {
    curTimeZone = 'Владивостокское время (MSK+7)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 11) {
    curTimeZone = 'Среднеколымское время (MSK+8)';
    // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  } else if (hoursDiffStdTime === 12) {
    curTimeZone = 'Камчатское время (MSK+9)';
  } else {
    curTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  }

  return curTimeZone;
};
