import * as H from 'history';
export interface IUserBlockProps {
  history: H.History;
  userId: string;
}
