import React, { useCallback, useEffect, useMemo, useState } from 'react';

import { NO_MSISDN_ERROR_MESSAGE } from '@app/store/messages/constants';
import { formatURLForHistory } from '@app/utils/formatters';
import { useIsB2b } from '@app/utils/hooks/customHooks';
import Otp from '@components/Otp/Otp';
import Platforms from '@components/Platforms/Platforms';
import Profile from '@components/Profile/Profile';
import { clearMessages, showError } from '@store/messages/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import { IUserBlockProps } from './interfaces';

const UserBlock = ({ userId, history }: IUserBlockProps): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const { msisdn } = useLocalSelector((state) => state.user.userData.list[userId]);
  const [currentTab, setCurrentTab] = useState('profile');
  const isB2b = useIsB2b();

  const onTab = useCallback(
    (e: React.SyntheticEvent, tabName: string): void => {
      e.preventDefault(); // Отменили нативное действие

      // переключаем вкладку
      setCurrentTab(tabName);
    },
    [setCurrentTab]
  );

  const renderTab = useMemo((): React.ReactElement => {
    const tabs = {
      profile: <Profile history={history} userId={userId} />,
      otp: <Otp userId={userId} />,
      platforms: <Platforms userId={userId} />,
    };
    return <div className="tab-pane  active">{tabs[currentTab] || tabs.profile}</div>;
  }, [currentTab, history, userId]);

  const handleGoToHistory = useCallback(() => {
    if (!msisdn) {
      dispatch(showError({ msg: NO_MSISDN_ERROR_MESSAGE }));
      return;
    }
    dispatch(clearMessages());
    history.push(formatURLForHistory(msisdn));
  }, [dispatch, history, msisdn]);

  useEffect(() => {
    setCurrentTab('profile');
  }, [isB2b, setCurrentTab]);

  const getUserProfile = (): React.ReactElement => (
    <div className="row">
      <ul className="nav nav-tabs info-tabs" data-tabs="tabs">
        <li className={currentTab === 'profile' ? 'active' : ''}>
          <a data-toggle="tab" href="" onClick={(e) => onTab(e, 'profile')}>
            <i className="fa fa-user-circle fa-lg" aria-hidden="true" /> &nbsp; Информация о пользователе
          </a>
        </li>
        {isB2b && (
          <li className={currentTab === 'otp' ? 'active' : ''}>
            <a data-toggle="tab" href="" onClick={(e) => onTab(e, 'otp')}>
              <i className="fa fa-mobile-alt fa-lg" aria-hidden="true" /> &nbsp; Настройки SMS
            </a>
          </li>
        )}
        {isB2b && (
          <li className={currentTab === 'platforms' ? 'active' : ''}>
            <a data-toggle="tab" href="" onClick={(e) => onTab(e, 'platforms')}>
              <i className="fa fa-address-book fa-lg" aria-hidden="true" /> &nbsp; Продуктовые платформы
            </a>
          </li>
        )}
      </ul>

      <div className="tab-content">{renderTab}</div>
    </div>
  );

  return (
    <div>
      {getUserProfile()}
      <br /> <br />
      <div className="container">
        <div className="row">
          <button type="button" className="btn btn-info  btn-lg" onClick={handleGoToHistory}>
            <i className="fa fa-history fa-lg" aria-hidden="true" />
            &nbsp; История операций
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserBlock;
