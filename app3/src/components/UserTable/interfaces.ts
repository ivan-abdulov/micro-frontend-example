import { IGetUserData } from '@app/utils/api/user/interfaces';

export interface IUserTableProps {
  onBlock: (isPermanently: boolean) => () => void;
  onDelete: () => void;
  onImpersonateUser: () => void;
  onUnlock: () => void;
  userData: IGetUserData;
}

export interface IBlockedUserTableProps {
  onDelete: () => void;
  onUnlock: () => void;
  userData: IGetUserData;
}
