import React from 'react';

import { blockedTimeFormatter, dateFormatter, formatSourceName } from '@utils/formatters';

import BlockList from '../BlockList/BlockList';
import { IBlockedUserTableProps } from './interfaces';

const BlockedUserTable = ({ onUnlock, onDelete, userData }: IBlockedUserTableProps): React.ReactElement => (
  <div>
    <div className="row">
      <div className="col-md-6">
        <table className="table table-striped table-bordered">
          <tbody>
            {userData.msisdn ? (
              <tr>
                <td>
                  <b>Телефон</b>
                </td>
                <td>{userData.msisdn}</td>
              </tr>
            ) : null}
            {userData.displayFN ? (
              <tr>
                <td>
                  <b>ФИО</b>
                </td>
                <td>{userData.displayName || `${userData.firstName} ${userData.lastName}`}</td>
              </tr>
            ) : null}
            {userData.email ? (
              <tr>
                <td>
                  <b>E-mail</b>
                </td>
                <td>{userData.email}</td>
              </tr>
            ) : null}
            <tr>
              <td>
                <b>Заблокирован?</b>
              </td>
              <td>Да</td>
            </tr>
            <tr>
              <td>
                <b>Источник блокировки</b>
              </td>
              <td>{formatSourceName(userData.blockerId)}</td>
            </tr>
            {userData.blockCode ? (
              <tr>
                <td>
                  <b>Код</b>
                </td>
                <td>{userData.blockCode}</td>
              </tr>
            ) : null}
            {userData.blockName ? (
              <tr>
                <td>
                  <b>Описание</b>
                </td>
                <td>{userData.blockName}</td>
              </tr>
            ) : null}
            <tr>
              <td>
                <b>Создана</b>
              </td>
              <td>{dateFormatter(userData.blockFd)}</td>
            </tr>
            {userData.blockedTo ? (
              <tr>
                <td>
                  <b>Заблокирован до</b>
                </td>
                <td>{dateFormatter(userData.blockedTo)}</td>
              </tr>
            ) : null}
            {userData.blockedTo ? (
              <tr key={10}>
                <td>
                  <b>Время разблокировки</b>
                </td>
                <td>{blockedTimeFormatter(userData.blockedTo)}</td>
              </tr>
            ) : null}
          </tbody>
        </table>
      </div>
    </div>

    <div className="row">
      <div className="col-md-11">
        {userData.blocks && userData.blocks.length > 0 && (
          <BlockList onUnlock={onUnlock} onDelete={onDelete} userData={userData} />
        )}
      </div>
    </div>
  </div>
);

export default BlockedUserTable;
