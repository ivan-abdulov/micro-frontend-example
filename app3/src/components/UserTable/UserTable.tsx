import React from 'react';

import BlockedUserTable from '@components/UserTable/BlockedUserTable';

import BlockedButtons from '../BlockedButtons/BlockedButtons';
import { IUserTableProps } from './interfaces';

const UserTable = ({
  onUnlock,
  onDelete,
  onBlock,
  onImpersonateUser,
  userData,
}: IUserTableProps): React.ReactElement => {
  if (userData.isBlocked) {
    return <BlockedUserTable onDelete={onDelete} onUnlock={onUnlock} userData={userData} />;
  }

  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <table className="table table-striped table-bordered">
            <tbody>
              {userData.msisdn ? (
                <tr>
                  <td>
                    <b>Телефон</b>
                  </td>
                  <td>{userData.msisdn}</td>
                </tr>
              ) : null}
              {userData.displayFN ? (
                <tr>
                  <td>
                    <b>ФИО</b>
                  </td>
                  <td>{userData.displayName || `${userData.firstName} ${userData.lastName}`}</td>
                </tr>
              ) : null}
              {userData.email ? (
                <tr>
                  <td>
                    <b>E-mail</b>
                  </td>
                  <td>{userData.email}</td>
                </tr>
              ) : null}
              <tr>
                <td>
                  <b>Заблокирован?</b>
                </td>
                <td>Нет</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <BlockedButtons onBlock={onBlock} onDelete={onDelete} onImpersonateUser={onImpersonateUser} userData={userData} />
    </>
  );
};

export default UserTable;
