import React, { useCallback, useMemo } from 'react';

import { updateCurrentRealm } from '@app/store/login/slice';
import { ressetUserState } from '@app/store/user/slice';
import { ROLE_B2B, ROLE_B2C } from '@app/utils/api/login/constants';
import { useFormattedRealms } from '@app/utils/hooks/customHooks';
import { useLocalDispatch, useLocalSelector } from '@app/utils/hooks/useRedux';
import { Routes } from '@app/utils/routes/routesMap';

import styles from './RoleSelector.module.scss';

const RoleSelector: React.FC = () => {
  const { currentRealm, orgRoles: rolesFromState } = useLocalSelector((state) => state.login);
  const realms = useFormattedRealms();
  const dispatch = useLocalDispatch();
  const handleSelectRole = useCallback(
    (e) => {
      dispatch(updateCurrentRealm({ realm: e.target.value }));
      dispatch(ressetUserState());
      window.location.hash = `#${Routes.home}`;
    },
    [dispatch]
  );
  const isNotContanedBothTypesOfRoles = useMemo(
    () => !rolesFromState || !rolesFromState[ROLE_B2B] || !rolesFromState[ROLE_B2C],
    [rolesFromState]
  );

  if (isNotContanedBothTypesOfRoles) {
    return null;
  }

  return (
    <div className="container">
      <div className="row">
        <div className={styles.wrapper}>
          <div className={styles.selectWrapper}>
            <select
              className="form-control"
              id="roleSelectorInput"
              name="role_selector_input"
              value={currentRealm}
              onChange={handleSelectRole}>
              {realms.map(({ title, value }) => (
                <option value={value} key={value}>
                  {title}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RoleSelector;
