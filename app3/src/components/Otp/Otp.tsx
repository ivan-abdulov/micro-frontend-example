import React, { ReactElement, SyntheticEvent, useCallback, useEffect, useMemo, useState } from 'react';

import { showError, showSuccess } from '@store/messages/slice';
import API from '@utils/api';
import { useLocalDispatch } from '@utils/hooks/useRedux';

import { IOTPProps } from './interfaces';

const Otp = ({ userId }: IOTPProps): React.ReactElement => {
  const [onFactor, setOnFactor] = useState(false);
  const [isErrorFactor, setIsErrorFactor] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useLocalDispatch();
  useEffect((): void => {
    void (async () => {
      setIsLoading(true);
      setIsErrorFactor(false);
      try {
        const { data } = await API.user.get2Factor(userId);

        if (typeof data !== 'boolean') {
          throw new Error();
        }

        setOnFactor(data);
      } catch (e) {
        setIsErrorFactor(true);
      }
      setIsLoading(false);
    })();
  }, [userId]);

  const onfactor = useCallback(
    (f: boolean) => (e: SyntheticEvent): void => {
      e.preventDefault();
      setOnFactor(f);
    },
    [setOnFactor]
  );

  const sendfactor = useCallback(
    async (e: SyntheticEvent): Promise<void> => {
      e.preventDefault(); // Отменили нативное действие
      setIsLoading(true);
      try {
        await API.user.put2Factor(userId, onFactor);
        dispatch(showSuccess({ msg: 'Настройки SMS сохранены.' }));
      } catch (error) {
        dispatch(showError({ msg: 'Ошибка сохранения настроек!' }));
      }
      setIsLoading(false);
    },
    [dispatch, onFactor, userId]
  );

  const getFactorButton = useMemo((): ReactElement => {
    if (isErrorFactor) {
      return <div className="factor-error">Ошибка загрузки признака.</div>;
    } // end if

    if (onFactor) {
      return (
        <i
          onClick={onfactor(false)}
          className="fa fa-toggle-on fa-2x on-2-factor"
          aria-hidden="true"
          title="выключить"
        />
      );
    } // end if

    return (
      <i onClick={onfactor(true)} className="fa fa-toggle-off fa-2x off-2-factor" aria-hidden="true" title="включить" />
    );
  }, [isErrorFactor, onFactor, onfactor]);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6">
          <table className="table table-striped table-bordered">
            <tbody>
              <tr>
                <td>
                  <b>Вход по логину и паролю</b>
                </td>
                <td className="factor-on-td">{isLoading ? <div>загрузка...</div> : getFactorButton}</td>
              </tr>
            </tbody>
          </table>

          <button className="btn btn-primary" onClick={sendfactor}>
            Применить
          </button>
          <br />
          <br />
        </div>
      </div>
    </div>
  );
};

export default Otp;
