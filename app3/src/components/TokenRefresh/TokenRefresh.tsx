import React, { useEffect } from 'react';

import { tokenRefreshThunk, updateRefreshExpiresIn } from '@store/login/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import { TOKEN_INTERVAL_S, TOKEN_REFRESH_GAP } from './constants';

const TokenRefresh: React.FC = (): null => {
  const { refreshExpiresIn, isAuthenticated, expires_in } = useLocalSelector((state) => state.login);
  const dispatch = useLocalDispatch();
  useEffect(() => {
    if (!isAuthenticated || !expires_in) {
      return;
    }

    const id = setInterval(() => {
      const newRefreshExpiresIn = refreshExpiresIn - TOKEN_INTERVAL_S;
      // console.log(
      //   'До обновления токена осталось:',
      //   newRefreshExpiresIn - TOKEN_REFRESH_GAP,
      //   'секунд, попытка №',
      //   refreshTries + 1,
      //   'из ',
      //   TOKEN_REFRESH_COUNT + 1
      // );

      dispatch(updateRefreshExpiresIn({ time: newRefreshExpiresIn }));
      if (newRefreshExpiresIn < TOKEN_REFRESH_GAP) {
        dispatch(tokenRefreshThunk());
      }
    }, TOKEN_INTERVAL_S * 1000);

    return () => id && clearInterval(id);
  }, [dispatch, refreshExpiresIn, isAuthenticated, expires_in]);

  return null;
};

export default TokenRefresh;
