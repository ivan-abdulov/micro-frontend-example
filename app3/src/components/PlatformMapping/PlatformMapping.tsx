import cn from 'classnames';
import moment from 'moment';
import React from 'react';

import { DATE_FORMAT } from '@utils/constants';

import styles from './PlatformMapping.module.scss';
import { IPlatformProps } from './interfaces';

const PlatformMapping = ({ val, handleRemove }: IPlatformProps): React.ReactElement => (
  <div className="btn-group">
    <div className={styles.btnPhone} title={'Создана: ' + moment(val.created).format(DATE_FORMAT)}>
      <div className={styles.desc}>Создана: {moment(val.created).format(DATE_FORMAT)}</div>
      {val.phonenum}
    </div>
    <button type="button" className={cn('btn btn-danger', styles.btnDanger)} onClick={handleRemove(val)}>
      <i className="fa fa-trash" aria-hidden="true" />
    </button>
  </div>
);

export default PlatformMapping;
