import { Ival } from '@store/user/interfaces';

export interface IPlatformProps {
  handleRemove: (val: Ival) => () => void;
  val: Ival;
}
