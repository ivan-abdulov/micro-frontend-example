import ru from 'date-fns/locale/ru';
import React, { useCallback } from 'react';
import DatePicker from 'react-datepicker';

import { showError } from '@app/store/messages/slice';
import { WRONG_PARAMS } from '@app/utils/api/user/constants';
import { changeBlockedToPicker } from '@store/user/slice';
import { useLocalDispatch } from '@utils/hooks/useRedux';

import { IBlockedButtons } from './interfaces';

const BlockedButtons = ({ onBlock, onDelete, onImpersonateUser, userData }: IBlockedButtons): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const handleChangeBlockedToPicker = useCallback(
    (blockedToPicker: Date): void => {
      if (!userData.id) {
        dispatch(showError({ msg: WRONG_PARAMS }));
        return;
      }

      dispatch(
        changeBlockedToPicker({
          userId: userData.id,
          blockedToPicker: blockedToPicker.getTime(),
        })
      );
    },
    [dispatch, userData.id]
  );

  return (
    <div className="blocked-panel">
      <div className="row">
        <div className="col-sm-6 col-md-5 col-lg-3">
          <div className="form-group">
            <b>Дата/время окончания блокировки</b>
            <label className="input-group date-blocked" id="blockedTo">
              <DatePicker
                className="form-control js-start input-sf"
                id={`blockedToPicker_${userData.id}`}
                name="blockedToPicker"
                selected={userData.blockedToPicker ? new Date(userData.blockedToPicker) : new Date()}
                onChange={handleChangeBlockedToPicker}
                locale={ru}
                dateFormat="dd.MM.yyyy HH:mm"
                showTimeSelect
              />

              <span className="input-group-addon">
                <i className="fa fa-calendar" aria-hidden="true" />
              </span>
            </label>
          </div>
        </div>
        <div className="col-sm-6 col-md-5 col-lg-3">
          <button
            disabled={!userData.blockedToPicker}
            type="button"
            className="btn btn-warning btn-temp-block"
            onClick={onBlock(false)}>
            <i className="fa fa-clock" aria-hidden="true" />
            &nbsp; Заблокировать временно
          </button>
        </div>
      </div>

      <div className="row red-buttons">
        <div className="col-sm-6 col-md-5 col-lg-3">
          <button type="button" className="btn btn-danger" onClick={onBlock(true)}>
            <i className="fa fa-lock" aria-hidden="true" />
            &nbsp; Заблокировать бессрочно
          </button>
          &nbsp;
        </div>
        <div className="col-sm-6 col-md-5 col-lg-3">
          <button type="button" className="btn btn-danger btn-del-user" onClick={onDelete}>
            <i className="fa fa-trash" aria-hidden="true" />
            &nbsp; Удалить
          </button>
        </div>
        <div className="col-sm-12 col-md-12 col-lg-6">
          <div className="visible-sm visible-md">&nbsp;</div>
          <button type="button" className="btn btn-success btn-impersonate-user" onClick={onImpersonateUser}>
            Войти от имени пользователя
          </button>
        </div>
      </div>
    </div>
  );
};

export default BlockedButtons;
