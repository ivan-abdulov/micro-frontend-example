import { IGetUserData } from '@app/utils/api/user/interfaces';

export interface IBlockedButtons {
  onBlock: (isPermanently: boolean) => () => void;
  onDelete: () => void;
  onImpersonateUser: () => void;
  userData: IGetUserData;
}
