import * as H from 'history';

export interface ISearchFormProps {
  history: H.History;
  search_email?: string;
  search_msisdn?: string;
  search_platform?: string;
  search_platform_value?: string;
}

export interface ISearchFormValues {
  search_email: string;
  search_msisdn: string;
  search_platform: string;
  search_platform_value: string;
}
