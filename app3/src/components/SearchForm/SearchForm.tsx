import cn from 'classnames';
import React, { useCallback, useMemo } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { useForm } from 'react-hook-form';

import { makeSearchUrl } from '@app/utils/api/user/utils';
import { VALIDATION_PATTERNS } from '@app/utils/constants';
import { useIsB2b } from '@app/utils/hooks/customHooks';
import { clearMessages, showError } from '@store/messages/slice';
import { loadUserData, setSearchParams } from '@store/user/slice';
import { useLocalDispatch } from '@utils/hooks/useRedux';

import styles from './SearchForm.module.scss';
import { ISearchFormProps, ISearchFormValues } from './interfaces';
import { isFormValidUtil, preparedPlatforms } from './utils';

const formGroupClass = 'form-group';
const inputClasses = 'form-control js-msisdn input-sf';
const inputClassesError = 'form-control js-msisdn input-sf alert-f';

const NONE_SEARCH_PP_VALUE = 'none';

const SearchForm = ({
  history,
  search_msisdn,
  search_email,
  search_platform,
  search_platform_value,
}: ISearchFormProps): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const isB2b = useIsB2b();
  const {
    register,
    getValues,
    trigger,
    formState: { errors, dirtyFields },
    handleSubmit,
    watch,
  } = useForm<ISearchFormValues>({
    defaultValues: {
      search_email: search_email || '',
      search_msisdn: search_msisdn || '',
      search_platform: search_platform || NONE_SEARCH_PP_VALUE,
      search_platform_value: search_platform_value || '',
    },
    mode: 'onChange',
    reValidateMode: 'onChange',
  });

  const values = getValues();
  const isFormValid = useMemo(() => isFormValidUtil(values, errors), [errors, values]);

  const onSearchFormSubmit = useCallback(
    (formValues: ISearchFormValues): void => {
      try {
        const url = makeSearchUrl(formValues);
        if (history.location.pathname === url) {
          const params = {
            search_msisdn: formValues.search_msisdn,
            search_email: formValues.search_email,
            search_platform: formValues.search_platform,
            search_platform_value: formValues.search_platform_value,
          };
          dispatch(clearMessages());
          dispatch(setSearchParams(params));
          void dispatch(loadUserData(params));
        } else {
          history.push(url);
        }
      } catch (e) {
        dispatch(showError({ msg: e.message }));
      }
    },
    [dispatch, history]
  );

  return (
    <div>
      <div className="search-error js-search-error" />
      <div className="row">
        <div className="col-md-12">
          <div className={styles.searchFormWrapper}>
            <form
              className={cn('form-inline', styles.searchForm, { [styles.searchFormB2c]: !isB2b })}
              onSubmit={handleSubmit(onSearchFormSubmit)}>
              <div className={cn(styles.formGroup, formGroupClass)}>
                <label htmlFor="search_msisdn">
                  <b>MSISDN:</b>
                </label>
                <input
                  type="text"
                  className={errors.search_msisdn && dirtyFields.search_msisdn ? inputClassesError : inputClasses}
                  id="search_msisdn"
                  placeholder="напр. 9211110500"
                  name="search_msisdn"
                  ref={register({ pattern: VALIDATION_PATTERNS.msisdn })}
                  disabled={!!watch('search_email') || !!watch('search_platform_value')}
                />
              </div>
              <div className={cn(styles.formGroup, formGroupClass)}>
                <label htmlFor="search_email">
                  <b>или E-mail:</b>
                </label>
                <input
                  type="text"
                  className={cn(
                    styles.emailInput,
                    errors.search_email && dirtyFields.search_email ? inputClassesError : inputClasses
                  )}
                  id="search_email"
                  name="search_email"
                  placeholder="напр. test@gmail.com"
                  ref={register({ pattern: VALIDATION_PATTERNS.email })}
                  disabled={!!watch('search_msisdn') || !!watch('search_platform_value')}
                />
              </div>
              {isB2b && (
                <div className={cn(styles.formGroup, formGroupClass)}>
                  <label htmlFor="search_platform">
                    <b>или Продуктовая платформа:</b>
                  </label>
                  <div className={styles.ppInputWrapper}>
                    <select
                      className={cn('form-control', errors.search_platform ? inputClassesError : inputClasses)}
                      disabled={!!watch('search_msisdn') || !!watch('search_email')}
                      id="search_platform"
                      name="search_platform"
                      placeholder="выберите ПП"
                      ref={register({
                        validate: (value) => (watch('search_platform_value') ? value !== NONE_SEARCH_PP_VALUE : true),
                      })}
                      onChange={() => {
                        trigger('search_platform_value');
                      }}>
                      <option disabled value="none" key="none">
                        выберите ПП
                      </option>
                      {Object.values(preparedPlatforms).map(({ title, value }) => (
                        <option value={value} key={value}>
                          {title}
                        </option>
                      ))}
                    </select>
                    <input
                      type="text"
                      className={cn(
                        styles.ppInput,
                        errors.search_platform_value && dirtyFields.search_platform_value
                          ? inputClassesError
                          : inputClasses
                      )}
                      id="search_platform_value"
                      name="search_platform_value"
                      placeholder="напр. 9211110500 или test@gmail.com"
                      disabled={!!watch('search_msisdn') || !!watch('search_email')}
                      ref={register({
                        validate: (value) =>
                          value ? preparedPlatforms[watch('search_platform')]?.validationPattern.test(value) : true,
                      })}
                      onChange={() => {
                        trigger('search_platform');
                      }}
                    />
                  </div>
                </div>
              )}
              <div className={styles.buttonsWrapper}>
                <button className="btn btn-primary js-history" type="submit" disabled={!isFormValid}>
                  <i className="fa fa-search" aria-hidden="true" />
                  &nbsp; Найти
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchForm;
