import { DeepMap, FieldError } from 'react-hook-form';

import { platforms } from '@app/store/user/constants';
import { IPlatform } from '@app/store/user/interfaces';
import { VALIDATION_PATTERNS } from '@app/utils/constants';

import { ISearchFormValues } from './interfaces';

const validationPatterns = { ...VALIDATION_PATTERNS };
validationPatterns.phone = /^\d{11}$/;

export const preparedPlatforms: {
  [key: string]: { title: string; validationPattern: RegExp; value: string };
} = Object.values(platforms).reduce((acc, side) => {
  side.forEach(({ name, code, type }: IPlatform) => {
    acc[code] = {
      title: name,
      value: code,
      validationPattern: new RegExp(validationPatterns[type]),
    };
  });
  return acc;
}, {});

export const isFormValidUtil = (values: ISearchFormValues, errors: DeepMap<ISearchFormValues, FieldError>): boolean =>
  (!errors.search_msisdn && Boolean(values.search_msisdn)) ||
  (!errors.search_email && Boolean(values.search_email)) ||
  (values.search_platform !== 'none' && !errors.search_platform_value && Boolean(values.search_platform_value));
