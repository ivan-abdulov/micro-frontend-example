import { IGetUserData } from '@app/utils/api/user/interfaces';

export interface IBlockListProps {
  onDelete: () => void;
  onUnlock: () => void;
  userData: IGetUserData;
}
