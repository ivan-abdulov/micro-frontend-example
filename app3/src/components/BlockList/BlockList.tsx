import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import { blockedTimeFormatter, dateFormatter, formatSourceName } from '@utils/formatters';

import { BLOCKER_CALL_CENTER_ID } from './constants';
import { IBlockListProps } from './interfaces';

const BlockList = ({ onUnlock, onDelete, userData }: IBlockListProps): React.ReactElement => (
  <div>
    <div className="lock-but">
      {userData?.blockerId === BLOCKER_CALL_CENTER_ID && (
        <>
          <button type="button" className="btn btn-primary" onClick={onUnlock}>
            <i className="fa fa-unlock-alt" aria-hidden="true" />
            &nbsp; Разблокировать
          </button>
          &nbsp;&nbsp;
        </>
      )}
      <button type="button" className="btn btn-danger btn-del-user" onClick={onDelete}>
        <i className="fa fa-trash" aria-hidden="true" />
        &nbsp; Удалить
      </button>
    </div>

    {userData.blocks && userData.blocks.length ? (
      <div>
        <h4>&nbsp;Список блокировок</h4>

        <BootstrapTable data={userData.blocks} striped hover>
          <TableHeaderColumn isKey dataField="blockerId" dataFormat={formatSourceName}>
            Источник блокировки
          </TableHeaderColumn>
          <TableHeaderColumn dataField="code">Код</TableHeaderColumn>
          <TableHeaderColumn dataField="name">Описание</TableHeaderColumn>

          <TableHeaderColumn dataField="fd" dataFormat={dateFormatter}>
            Создана
          </TableHeaderColumn>
          <TableHeaderColumn dataField="blockedTo" dataFormat={dateFormatter}>
            Заблокирован до
          </TableHeaderColumn>
          <TableHeaderColumn dataField="blockedTo" dataFormat={blockedTimeFormatter}>
            Время разблокировки
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    ) : (
      ''
    )}
  </div>
);
export default BlockList;
