import React, { SyntheticEvent, useCallback, useEffect } from 'react';
import { Controller } from 'react-hook-form';
import { useForm } from 'react-hook-form/dist/index.ie11';
import InputMask from 'react-input-mask';

import { VALIDATION_PATTERNS } from '@app/utils/constants';
import Loader from '@components/Loader/Loader';
import { clearMessages } from '@store/messages/slice';
import { registerNewUser } from '@store/user/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import { IRegistrationFormValues } from './interfaces';

const ERRORED_CLASSES = 'form-control alert-f';
const NORMAL_CLASSES = 'form-control';

const RegistrationForm = (): React.ReactElement => {
  const dispatch = useLocalDispatch();
  const isLoading = useLocalSelector((state) => state.user.registerForm.isLoading);
  const {
    register,
    control,
    errors,
    formState: { isValid },
    handleSubmit,
  } = useForm<IRegistrationFormValues>({
    defaultValues: { email: '', firstName: '', lastName: '', msisdn: '' },
    mode: 'onChange',
    reValidateMode: 'onChange',
  });

  useEffect(() => {
    dispatch(clearMessages());
  }, [dispatch]);

  const cancel = (e: SyntheticEvent): void => {
    e.preventDefault();

    window.history.back();
  };

  const onSubmit = useCallback(
    (values: IRegistrationFormValues): void => {
      const re = new RegExp(' |\\(|\\)|\\+|\\-', 'gi');

      const sendData = {
        msisdn: values.msisdn.replace(re, ''),
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
      };
      void dispatch(registerNewUser(sendData));
    },
    [dispatch]
  );

  return (
    <div className="container">
      <div className="reg-content">
        <div className="row">
          <div className="col-md-12">
            <h2>Регистрация МегаФон ID</h2>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            {isLoading && <Loader />}
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group">
                <label htmlFor="msisdn">Телефон</label>
                <Controller
                  name="msisdn"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: true,
                    pattern: VALIDATION_PATTERNS.phone,
                  }}
                  render={(field, { invalid }) => (
                    <InputMask
                      mask="+7 (999) 999-99-99"
                      className={invalid ? 'form-control js-mask alert-f' : 'form-control js-mask'}
                      id="msisdn"
                      placeholder="+7 (___) ___-__-__"
                      {...field}
                    />
                  )}
                />
              </div>

              <div className="form-group">
                <label htmlFor="firstName">Имя</label>
                <input
                  type="text"
                  className={errors.firstName ? ERRORED_CLASSES : NORMAL_CLASSES}
                  id="firstName"
                  name="firstName"
                  ref={register({ required: true })}
                />
              </div>

              <div className="form-group">
                <label htmlFor="lastName">Фамилия</label>
                <input
                  type="text"
                  className={errors.lastName ? ERRORED_CLASSES : NORMAL_CLASSES}
                  id="lastName"
                  name="lastName"
                  ref={register({ required: true })}
                />
              </div>

              <div className="form-group">
                <label htmlFor="email">E-mail</label>
                <input
                  type="text"
                  className={errors.email ? ERRORED_CLASSES : NORMAL_CLASSES}
                  id="email"
                  name="email"
                  ref={register({
                    pattern: VALIDATION_PATTERNS.email,
                    required: true,
                  })}
                />
              </div>

              <div className="form-group">
                <br />
                <button type="button" className="btn btn-default" onClick={cancel}>
                  Отмена
                </button>{' '}
                &nbsp;&nbsp;
                <button type="submit" className="btn btn-primary" disabled={!isValid}>
                  Зарегистрировать
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegistrationForm;
