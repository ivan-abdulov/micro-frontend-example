export interface IRegistrationFormValues {
  email: string;
  firstName: string;
  lastName: string;
  msisdn: string;
}
