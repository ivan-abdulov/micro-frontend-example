import * as H from 'history';
import React from 'react';

export interface IProfileProps {
  history: H.History;
  userId: string;
}

export interface IConfirmParams {
  confirmCallback: (() => Promise<void> | void) | null;
  confirmTitle: string;
  showConfirm: boolean;
  confirmText?: React.ReactElement | string | null;
}
