import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import 'moment/locale/ru';

import { unwrapResult } from '@reduxjs/toolkit';

import configAdapter from '@app/configs/config';
import { REALM_B2B, REALM_B2C, ROLE_B2B, ROLE_B2C } from '@app/utils/api/login/constants';
import { WRONG_PARAMS } from '@app/utils/api/user/constants';
import { useIsB2b } from '@app/utils/hooks/customHooks';
import ConfirmModal from '@components/ConfirmModal/ConfirmModal';
import UserTable from '@components/UserTable/UserTable';
import { clearMessages, showError, showSuccess } from '@store/messages/slice';
import { blockUser, deleteUser, unblockUser } from '@store/user/slice';
import API from '@utils/api';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import { IConfirmParams, IProfileProps } from './interfaces';

moment.locale('ru');

const Profile = ({ history, userId }: IProfileProps): React.ReactElement => {
  const userData = useLocalSelector((state) => state.user.userData.list[userId]);
  const dispatch = useLocalDispatch();
  const [confirmParams, setConfirmParams] = useState<IConfirmParams>({
    showConfirm: false,
    confirmTitle: '',
    confirmCallback: null,
  });
  const isB2b = useIsB2b();
  const handleCloseConfirm = useCallback(() => {
    setConfirmParams({
      showConfirm: false,
      confirmTitle: '',
      confirmCallback: null,
    });
  }, []);

  const impersonateUser = useCallback(
    async (impersonate_msisdn: string): Promise<void> => {
      try {
        const { data } = await API.user.getJWT(impersonate_msisdn, isB2b ? REALM_B2B : REALM_B2C);
        const sso_base_url = configAdapter('sso_base_url');
        const links = {
          // TODO Update url for each role.
          [ROLE_B2C]: `${sso_base_url}/impersonate-customer?jwt=${data}`,
          [ROLE_B2B]: `${sso_base_url}/impersonate-b2b?jwt=${data}`,
        };
        window.location.href = isB2b ? links[ROLE_B2B] : links[ROLE_B2C];
      } catch (error) {
        dispatch(showError({ msg: 'Ошибка входа от имени пользователя' }));
      }
    },
    [dispatch, isB2b]
  );

  useEffect(() => {
    if (userData.impersonate_msisdn) {
      void impersonateUser(userData.impersonate_msisdn);
    }
  }, [impersonateUser, userData.impersonate_msisdn]);

  const onBlock = useCallback(
    (isPermanently: boolean) => (): void => {
      setConfirmParams({
        showConfirm: true,
        confirmTitle: `Вы уверены, что хотите заблокировать пользователя ${userData.msisdn}?`,
        confirmText: !isPermanently ? (
          <>
            Дата/время окончания блокировки:{' '}
            <b>{userData.blockedToPicker ? moment(userData.blockedToPicker).format('DD.MM.yyyy HH:mm') : '--'}</b>
          </>
        ) : null,
        confirmCallback: async (): Promise<void> => {
          try {
            if (!userId || !userData.msisdn) {
              throw new Error();
            }
            dispatch(clearMessages());
            unwrapResult(
              await dispatch(
                blockUser({
                  userId,
                  isPermanently,
                })
              )
            );
            dispatch(showSuccess({ msg: 'Пользователь успешно заблокирован' }));
          } catch (error) {
            dispatch(showError({ msg: 'Ошибка блокировки пользователя' }));
          }
        },
      });
    },
    [dispatch, userData.blockedToPicker, userData.msisdn, userId]
  );

  const onDelete = useCallback((): void => {
    setConfirmParams({
      showConfirm: true,
      confirmTitle: `Вы уверены, что хотите удалить пользователя ${userData.msisdn}?`,
      confirmCallback: async () => {
        try {
          if (!userId || !userData.msisdn) {
            throw new Error(WRONG_PARAMS);
          }
          dispatch(clearMessages());
          unwrapResult(await dispatch(deleteUser(userId)));
          dispatch(showSuccess({ msg: 'Пользователь удалён' }));
          history.push('/home');
        } catch (error) {
          dispatch(showError({ msg: error.message || 'Ошибка удаления пользователя' }));
        }
      },
    });
  }, [dispatch, history, userData.msisdn, userId]);

  const onImpersonateUser = useCallback((): void => {
    try {
      if (!userData.msisdn) {
        throw new Error();
      }
      void impersonateUser(userData.msisdn);
    } catch (error) {
      dispatch(showError({ msg: 'Ошибка входа от имени пользователя' }));
    }
  }, [dispatch, impersonateUser, userData.msisdn]);

  const onUnlock = useCallback((): void => {
    setConfirmParams({
      showConfirm: true,
      confirmTitle: `Вы уверены, что хотите разблокировать пользователя ${userData.msisdn}?`,
      confirmCallback: async () => {
        try {
          if (!userData.id || !userData.msisdn) {
            throw new Error(WRONG_PARAMS);
          }
          dispatch(clearMessages());
          unwrapResult(await dispatch(unblockUser(userData.id)));
          dispatch(showSuccess({ msg: 'Пользователь разблокирован' }));
        } catch (error) {
          dispatch(showError({ msg: error.message || 'Ошибка разблокировки пользователя' }));
        }
      },
    });
  }, [dispatch, userData.msisdn]);

  return (
    <div className="container">
      <ConfirmModal {...confirmParams} closeCallback={handleCloseConfirm} />
      <UserTable
        onUnlock={onUnlock}
        onDelete={onDelete}
        onBlock={onBlock}
        onImpersonateUser={onImpersonateUser}
        userData={userData}
      />
    </div>
  );
};

export default Profile;
