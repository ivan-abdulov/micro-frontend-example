import cn from 'classnames';
import React from 'react';

import styles from './LoginForm.module.scss';

const Error = ({ error }: { error?: string }): React.ReactElement | null => {
  if (!error) {
    return null;
  }
  return (
    <div className="form-group">
      <div className={cn('col-sm-12', styles.inputWrapper)}>
        <span />
        <span className={styles.error}>{error}</span>
      </div>
    </div>
  );
};

export default Error;
