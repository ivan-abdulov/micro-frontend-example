import cn from 'classnames';
import React, { useCallback, useEffect, useMemo } from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { useForm } from 'react-hook-form';
import { FormattedMessage, useIntl } from 'react-intl';

import Loader from '@components/Loader/Loader';
import { ILoginFormValues } from '@pages/Login/interfaces';
import { getTokenRequestThunk } from '@store/login/slice';
import { formatConstrains, formatErrorsFromServer } from '@utils/formatters';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import Error from './Error';
import styles from './LoginForm.module.scss';

const ERRORED_CLASSES = 'form-control alert-f';
const NORMAL_CLASSES = 'form-control';

interface ILoginFormProps {
  loginParams: {
    client_id: string;
    client_secret: string;
    realm: string;
  };
}

const LoginForm: React.FC<ILoginFormProps> = ({ loginParams }) => {
  const { formatMessage } = useIntl();
  const dispatch = useLocalDispatch();
  const { loginLoading, form } = useLocalSelector((state) => state.login);
  const { setError, register, errors, handleSubmit } = useForm<ILoginFormValues>({
    defaultValues: {
      username: '',
      password: '',
    },
    mode: 'onChange',
  });
  const constrains = useMemo(
    () => ({
      username: formatConstrains(form?.fields?.username?.constraints),
      password: formatConstrains(form?.fields?.password?.constraints),
    }),
    [form]
  );
  const errorsFromServer = useMemo(() => formatErrorsFromServer(form?.errors), [form]);
  useEffect(() => {
    if (errorsFromServer?.username) {
      setError('username', { message: errorsFromServer?.username });
    }
    if (errorsFromServer?.password) {
      setError('password', { message: errorsFromServer?.password });
    }
  }, [errorsFromServer, setError]);

  const onSubmit = useCallback(
    (values: ILoginFormValues): void => {
      void dispatch(
        getTokenRequestThunk({
          ...values,
          ...loginParams,
        })
      );
    },
    [dispatch, loginParams]
  );

  return (
    <div className={`panel panel-default ${styles.loginForm}`}>
      {loginLoading && <Loader />}
      <div className="panel-heading">
        <i className="fa fa-sign-in" aria-hidden="true" />
        <FormattedMessage id="loginForm.title" />
      </div>
      <div className="panel-body">
        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-group">
            <div className={cn('col-sm-12', styles.inputWrapper)}>
              <label className="control-label" htmlFor="username">
                <FormattedMessage id="loginForm.login.label" />
              </label>
              <input
                autoComplete="username"
                id="username"
                type="text"
                name="username"
                placeholder={formatMessage({ id: 'loginForm.login.placeholder' })}
                className={errors.username ? ERRORED_CLASSES : NORMAL_CLASSES}
                ref={register({ ...constrains.username })}
              />
            </div>
          </div>
          {errorsFromServer?.username && <Error error={errorsFromServer?.username} />}
          <div className="form-group">
            <div className={cn('col-sm-12', styles.inputWrapper)}>
              <label className="control-label" htmlFor="password">
                <FormattedMessage id="loginForm.password.label" />
              </label>
              <input
                id="password"
                autoComplete="current-password"
                type="password"
                name="password"
                placeholder={formatMessage({ id: 'loginForm.password.placeholder' })}
                className={errors.password ? ERRORED_CLASSES : NORMAL_CLASSES}
                ref={register({ ...constrains.password })}
              />
            </div>
          </div>
          {errorsFromServer?.password && <Error error={errorsFromServer?.password} />}
          {errorsFromServer?.form && <Error error={errorsFromServer?.form} />}
          <div className="form-group">
            <div className={cn('col-sm-12', styles.inputWrapper)}>
              <div />
              <button type="submit" className="btn  btn-success">
                <i className="fa fa-key" aria-hidden="true" />
                <FormattedMessage id="loginForm.button" />
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginForm;
