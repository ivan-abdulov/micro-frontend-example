import React, { useEffect, useMemo } from 'react';
import { render } from 'react-dom';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';

import store from '@app/store';
import TokenRefresh from '@components/TokenRefresh/TokenRefresh';
import locales from '@locales/index';
import RouteMap from '@utils/routes';

import Main from './containers/Main';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import './res/css/main.css';

const App = (): React.ReactElement => {
  const locale = useMemo(() => 'ru', []);
  // Theme effect. When it will bee needed, change to handler.
  useEffect(() => {
    document.documentElement.className = '';
    document.documentElement.classList.add('theme-default');
  }, []);

  return (
    <Provider store={store}>
      <TokenRefresh />
      <HashRouter>
        <IntlProvider locale={locale} messages={locales[locale]}>
          <Main>
            <RouteMap />
          </Main>
        </IntlProvider>
      </HashRouter>
    </Provider>
  );
};

const renderApp = (): void => {
  const rootElement = document.getElementById('root');
  const renderedElement = <App />;

  render(renderedElement, rootElement);
};

renderApp();
