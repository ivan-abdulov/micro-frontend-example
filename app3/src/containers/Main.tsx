import React, { ReactElement, useEffect } from 'react';

import RoleSelector from '@app/components/RoleSelector/RoleSelector';
import Header from '@components/Header/Header';
import { getTokenInfoRequestThunk } from '@store/login/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

interface IMainProps {
  children: ReactElement;
}

const Main = (props: IMainProps): React.ReactElement => {
  const { messageParams } = useLocalSelector((state) => state.messages);
  const dispatch = useLocalDispatch();
  const { isAuthenticated } = useLocalSelector((state) => state.login);

  let styleMsg = {
    width: '100%',
    margin: '0',
  };
  if (messageParams.isCompactMSG) {
    styleMsg = {
      width: '530px',
      margin: '0 auto',
    };
  }

  useEffect(() => {
    dispatch(getTokenInfoRequestThunk());
  }, [dispatch]);

  return (
    <div className="contact-center-ui">
      <Header />
      {isAuthenticated && <RoleSelector />}
      <div className={messageParams.isError || messageParams.isSuccess ? 'message' : 'message hide'}>
        <div className={messageParams.isError ? 'row' : 'row hide'} style={styleMsg}>
          <div className="col-md-12">
            <div className="alert alert-danger" role="alert">
              <div>{messageParams.errorText}</div>
            </div>
          </div>
        </div>

        <div className={messageParams.isSuccess ? 'row' : 'row hide'} style={styleMsg}>
          <div className="col-md-12">
            <div className="alert alert-success" role="alert">
              <div>{messageParams.successText}</div>
            </div>
          </div>
        </div>
      </div>

      {props.children}
    </div>
  );
};

export default Main;
