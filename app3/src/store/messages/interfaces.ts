export interface IMessageParams {
  errorText?: string | null;
  isCompactMSG?: boolean | null;
  isError?: boolean | null;
  isSuccess?: boolean | null;
  successText?: string | null;
}

export interface IMessagesState {
  messageParams: IMessageParams;
}

export type TMessageParams = { msg: string; isCompactMSG?: boolean };
