import { createSlice } from '@reduxjs/toolkit';

import { IMessagesState, TMessageParams } from './interfaces';

const initialState: IMessagesState = {
  messageParams: {
    isSuccess: null,
    isError: null,
    successText: null,
    errorText: null,
    isCompactMSG: false,
  },
};

export const userSlice = createSlice({
  name: 'Messages',
  initialState,
  reducers: {
    showError(state, { payload }): void {
      const { msg, isCompactMSG }: TMessageParams = payload;
      state.messageParams = {
        isError: Boolean(msg),
        isSuccess: false,
        errorText: msg,
        successText: null,
        isCompactMSG,
      };
    },
    showSuccess(state, { payload }): void {
      const { msg, isCompactMSG }: TMessageParams = payload;
      state.messageParams = {
        isSuccess: Boolean(msg),
        isError: false,
        errorText: null,
        successText: msg,
        isCompactMSG,
      };
    },

    clearMessages(state): void {
      state.messageParams = initialState.messageParams;
    },
  },
});

export const {
  reducer,
  actions: { showError, showSuccess, clearMessages },
} = userSlice;

export default reducer;
