import { ROLE_B2B, ROLE_B2C } from '@app/utils/api/login/constants';
import { TConstrain, TError, TRealms, TRoles } from '@utils/api/login/interfaces';

export interface ILoginState {
  isAuthenticated: boolean;
  loginLoading: boolean;
  refreshExpiresIn: number;
  refreshTries: number;
  claims?: {
    contactEmail?: string;
    displayName?: string;
  };
  currentRealm?: TRealms;
  errors?: TError[];
  execution?: string;
  exp?: number;
  expires_in?: number;
  form?: {
    name: string;
    errors?: any;
    fields?: {
      password: {
        constraints: TConstrain[];
      };
      username: {
        constraints: TConstrain[];
      };
    };
  };
  mpt_expires_in?: number;
  orgRoles?: {
    [ROLE_B2B]?: string[];
    [ROLE_B2C]?: string[];
  };
  realm?: string;
  refresh_expires_in?: number;
  roles?: TRoles[];
  step?: string;
  token_type?: string;
  userName?: string;
}

export interface IUpdateRefreshExpiresInPayload {
  time: number;
}
export interface IGetTokenRequestThunkParams {
  password?: string;
  skipAutoLogin?: boolean;
  username?: string;
}

export interface ILogoutThunkParams {
  manual: boolean;
}
