import { ActionReducerMapBuilder, createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { getLogoutRedirectHash } from '@app/configs/utils';
import { TRootState } from '@app/store';
import { REALM_B2B, REALM_B2C, TOKEN_REFRESH_COUNT } from '@app/utils/api/login/constants';
import { Routes } from '@app/utils/routes/routesMap';
import { ressetUserState } from '@store/user/slice';
import API from '@utils/api';
import { IGetTokenRequest, TRealms } from '@utils/api/login/interfaces';

import {
  IGetTokenRequestThunkParams,
  ILoginState,
  ILogoutThunkParams,
  IUpdateRefreshExpiresInPayload,
} from './interfaces';

const initialState: ILoginState = {
  loginLoading: false,
  refreshExpiresIn: 0,
  refreshTries: 0,
  isAuthenticated: false,
};

export const getTokenRequestThunk = createAsyncThunk(
  'GET_TOKEN_REQUEST',
  async (params: IGetTokenRequestThunkParams | undefined = {}, { getState, dispatch }) => {
    const { login } = <TRootState>getState();

    const reqParams: IGetTokenRequest = {
      ...params,
    };
    if (login.execution && (params?.username || params?.password)) {
      reqParams.execution = login.execution;
      reqParams['_eventId'] = 'next';
    }
    const response = await API.login.getTokenRequest(reqParams);
    if ('token_type' in response) {
      dispatch(getTokenInfoRequestThunk());
    }
    return response;
  }
);

export const getTokenInfoRequestThunk = createAsyncThunk('GET_TOKEN_INFO_REQUEST', () =>
  API.login.getTokenInfoRequest()
);

export const logoutThunk = createAsyncThunk('LOGOUT', async (params: ILogoutThunkParams, { dispatch, getState }) => {
  const loginPage = getLogoutRedirectHash(params.manual);
  const {
    login: { isAuthenticated },
  } = <TRootState>getState();
  dispatch(ressetUserState());
  dispatch(ressetLoginState());
  if (isAuthenticated) {
    try {
      await API.login.logoutRequest({ logoutRedirectUrl: `${location.origin}${location.pathname}${loginPage}` });
    } catch (error) {
      console.error(error);
    }
  }
  location.hash = loginPage;
  return {};
});

export const tokenRefreshThunk = createAsyncThunk('TOKEN_REFRESH', async (args, { rejectWithValue, getState }) => {
  const {
    login: { refreshTries },
  } = <TRootState>getState();
  try {
    const {
      data: { expires_in },
    } = await API.login.tokenRefreshRequest();

    if (!expires_in) {
      throw new Error();
    }
    return { expires_in };
  } catch (error) {
    if (refreshTries >= TOKEN_REFRESH_COUNT) {
      window.location.hash = `#${Routes.logout}`;
    }
    return rejectWithValue(null);
  }
});

export const userSlice = createSlice({
  name: 'Login',
  initialState,
  reducers: {
    updateCurrentRealm(state, { payload }): void {
      const { realm }: { realm: TRealms } = payload;
      state.currentRealm = realm;
    },
    updateRefreshExpiresIn(state, { payload }): void {
      const { time }: IUpdateRefreshExpiresInPayload = payload;
      state.refreshExpiresIn = time;
    },
    ressetLoginState(state): void {
      state.isAuthenticated = false;
      delete state.expires_in;
      delete state.mpt_expires_in;
      delete state.refresh_expires_in;
      delete state.token_type;
      delete state.claims;
      delete state.exp;
      delete state.execution;
    },
  },
  extraReducers: (builder: ActionReducerMapBuilder<ILoginState>) => {
    builder.addCase(getTokenRequestThunk.pending, (state) => {
      state.loginLoading = true;
    });
    builder.addCase(getTokenRequestThunk.fulfilled, (state, { payload }) => {
      state.loginLoading = false;
      if ('token_type' in payload) {
        state.expires_in = payload.expires_in;
        state.mpt_expires_in = payload.mpt_expires_in;
        state.refresh_expires_in = payload.refresh_expires_in;
        state.token_type = payload.token_type;
        state.claims = payload.claims;
        state.isAuthenticated = true;
        state.execution = undefined;
        state.form = undefined;
        state.step = undefined;
      } else {
        state.execution = payload.execution;
        state.form = payload.form;
        state.step = payload.step;
        state.errors = payload.errors;
      }
    });
    builder.addCase(getTokenRequestThunk.rejected, (state) => {
      state.loginLoading = false;
    });
    builder.addCase(getTokenInfoRequestThunk.fulfilled, (state, data) => {
      state.isAuthenticated = true;
      state.expires_in = data.payload.attributes.expires_in;
      state.exp = data.payload.exp;
      state.realm = data.payload.attributes.realm;
      state.userName = data.payload.attributes.user_name;
      state.orgRoles = data.payload.attributes.orgRoles;
      state.currentRealm = Object.keys(data.payload.attributes.orgRoles || {}).find((realm) =>
        [REALM_B2B, REALM_B2C].includes(realm)
      ) as TRealms;
      state.refreshExpiresIn = data.payload.attributes.expires_in;
    });
    builder.addCase(tokenRefreshThunk.fulfilled, (state, { payload: { expires_in } }) => {
      state.expires_in = expires_in;
      state.refreshExpiresIn = expires_in;
      state.refreshTries = 0;
    });
    builder.addCase(tokenRefreshThunk.rejected, (state) => {
      state.refreshTries++;
    });
  },
});

export const {
  reducer,
  actions: { updateRefreshExpiresIn, ressetLoginState, updateCurrentRealm },
} = userSlice;

export default reducer;
