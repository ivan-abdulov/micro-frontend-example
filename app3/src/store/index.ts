import { createLogger } from 'redux-logger';

import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import rootReducer from './reducers';

const logger = createLogger({
  collapsed: true,
});

const middleware = getDefaultMiddleware().concat(logger);

const store = configureStore({
  reducer: rootReducer,
  middleware,
});

export type TRootState = ReturnType<typeof store.getState>;
export type TDispatch = typeof store.dispatch;

export default store;
