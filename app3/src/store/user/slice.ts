import moment from 'moment';

import { ActionReducerMapBuilder, PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { TRootState } from '@app/store';
import { SOMETHING_WENT_WRONG, WRONG_PARAMS } from '@app/utils/api/user/constants';
import { HTTP_STATUS_CODE_409 } from '@app/utils/axios/constants';
import { showError, showSuccess } from '@store/messages/slice';
import API from '@utils/api';
import { IGetUsersListData, IOperationAuditUrlData, IRegistrationFormValues } from '@utils/api/user/interfaces';

import { ILoadUserDataParams, IUserState, IaddPlatformLinkParams, Ival } from './interfaces';

export const loadUserData = createAsyncThunk(
  'LOAD_USER_DATA',
  async (params: ILoadUserDataParams, { getState, rejectWithValue }) => {
    const {
      login: { currentRealm },
    } = <TRootState>getState();

    if (!currentRealm) {
      return rejectWithValue({ error: WRONG_PARAMS });
    }
    const list = await API.user.getUserData({
      ...params,
      realm: currentRealm,
    });
    return {
      list,
    };
  }
);

export const removePlatformLink = createAsyncThunk(
  'REMOVE_PLATFORM_LINK',
  async ({ userId, val: { customerId, partnerId, externalUserId, phonenum } }: { userId: string; val: Ival }) => {
    const sendData = {
      uid: customerId,
      partnerId,
      externalUserId,
    };
    await API.user.deletePartnerMappings(sendData);
    return {
      partnerId,
      phonenum,
      userId,
    };
  }
);

export const addPlatformLink = createAsyncThunk(
  'ADD_PLATFORM_LINK',
  async ({ params, userId }: IaddPlatformLinkParams) => {
    const { customerId, partnerId, externalUserId } = params;
    const sendData = {
      customerId,
      partnerId,
      externalUserId,
      partnerType: 'extid',
    };
    const platform = await API.user.addPartnerMappings(sendData);
    return {
      platform,
      userId,
    };
  }
);

export const blockUser = createAsyncThunk(
  'BLOCK_USER',
  async (
    { isPermanently, userId }: { isPermanently: boolean; userId: string },
    { getState, rejectWithValue, dispatch }
  ) => {
    const {
      user: {
        userData: { list },
      },
    } = <TRootState>getState();
    const { blockedToPicker, msisdn, id } = list[userId];
    if ((!isPermanently && !blockedToPicker) || !userId || !id || !msisdn) {
      return rejectWithValue({});
    }
    await API.user.blockUser(
      id,
      isPermanently ? '' : moment(blockedToPicker).utc().format('YYYY-MM-DDTHH:mm:ss.000Z')
    );
    await dispatch(
      loadUserData({
        search_msisdn: msisdn,
      })
    );
    return {};
  }
);

export const unblockUser = createAsyncThunk(
  'UNBLOCK_USER',
  async (
    userId: string,
    { getState, rejectWithValue, dispatch }
  ) => {
    const {
      user: {
        userData: { list },
      },
    } = <TRootState>getState();
    const { msisdn, id } = list[userId];
    if (!id || !msisdn) {
      return rejectWithValue({});
    }
    await API.user.unblockUser(id);
    await dispatch(
      loadUserData({
        search_msisdn: msisdn,
      })
    );
    return {};
  }
);

export const deleteUser = createAsyncThunk(
  'DELETE_USER',
  async (userId: string, { getState, rejectWithValue, dispatch }) => {
    const {
      user: {
        userData: { list },
      },
    } = <TRootState>getState();
    const { msisdn } = list[userId];
    try {
      if (!msisdn) {
        throw new Error(WRONG_PARAMS);
      }
      await API.user.deleteUser(msisdn);
    } catch (e) {
      dispatch(showError({ msg: e.message || SOMETHING_WENT_WRONG }));
      return rejectWithValue({});
    }
    return {
      userId,
    };
  }
);

export const getOperationAudit = createAsyncThunk(
  'GET_OPERATION_AUDIT',
  async (params: IOperationAuditUrlData, { rejectWithValue }) => {
    try {
      return await API.user.getOperationAudit(params);
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);

export const registerNewUser = createAsyncThunk(
  'REGISTER_NEW_USER',
  async (params: IRegistrationFormValues, { dispatch }) => {
    try {
      await API.user.sendRegistrationForm(params);
      const successText = 'МФ ID зарегистрирован успешно! На указанный e-mail выслана ссылка подверждения email.';

      dispatch(showSuccess({ msg: successText, isCompactMSG: true }));
    } catch (e) {
      const message = e?.data?.error?.message;

      if (!e?.status || e?.status !== HTTP_STATUS_CODE_409 || !message) {
        dispatch(showError({ msg: 'Ошибка соединения.', isCompactMSG: true }));
      } else {
        if (new RegExp('^msisdn', 'gi').test(message)) {
          dispatch(
            showError({
              msg: 'Пользователь с данным номером уже зарегистрирован.',
              isCompactMSG: true,
            })
          );
        }

        if (new RegExp('^email|e-mail', 'gi').test(message)) {
          dispatch(
            showError({
              msg: 'Пользователь с данным email уже зарегистрирован.',
              isCompactMSG: true,
            })
          );
        }
      }
    }
  }
);

const initialState: IUserState = {
  userData: {
    list: {},
    search_start: '',
    search_end: '',
    page: 0,
    isLoading: false,
  },
  operationAuditData: {
    isLoading: false,
  },
  registerForm: {
    isLoading: false,
  },
};

export const userSlice = createSlice({
  name: 'User',
  initialState,
  reducers: {
    changeLoading(state, { payload }: PayloadAction<boolean>) {
      state.userData.isLoading = payload;
    },
    changeBlockedToPicker(
      state,
      {
        payload: { userId, blockedToPicker },
      }: PayloadAction<{ blockedToPicker: number | null | undefined; userId: string }>
    ) {
      state.userData.list[userId].blockedToPicker = blockedToPicker || Date.now();
    },
    ressetUserState(state) {
      state.userData = {
        search_msisdn: '',
        search_start: '',
        search_end: '',
        page: 0,
        isLoading: false,
        list: {},
      };
      state.operationAuditData = {
        isLoading: false,
      };
      state.registerForm = {
        isLoading: false,
      };
    },
    setSearchParams(state, { payload }: { payload: ILoadUserDataParams }) {
      state.userData.search_email = payload.search_email;
      state.userData.search_msisdn = payload.search_msisdn;
      state.userData.search_platform = payload.search_platform;
      state.userData.search_platform_value = payload.search_platform_value;
      state.userData.list = {};
    },
  },
  extraReducers: (builder: ActionReducerMapBuilder<IUserState>) => {
    builder.addCase(loadUserData.pending, (state) => {
      state.userData.isLoading = true;
      delete state.userData.error;
    });
    builder.addCase(
      loadUserData.fulfilled,
      (
        state,
        {
          payload,
        }: PayloadAction<{
          list: IGetUsersListData;
        }>
      ) => {
        state.userData.isLoading = false;
        state.userData.list = { ...state.userData.list, ...payload.list };
      }
    );
    builder.addCase(loadUserData.rejected, (state, { error }) => {
      state.userData.isLoading = false;
      state.userData.error = error.message;
    });

    builder.addCase(removePlatformLink.pending, (state) => {
      state.userData.isLoading = true;
    });
    builder.addCase(removePlatformLink.fulfilled, (state, { payload }) => {
      const { partnerId, phonenum, userId } = payload;
      state.userData.isLoading = false;
      const partnerMappingsList = (state.userData.list[userId].partnerMappings || [])[partnerId];
      (state.userData.list[userId].partnerMappings || [])[partnerId] = partnerMappingsList.filter(
        (item: Ival) => item.phonenum !== phonenum
      );
    });
    builder.addCase(removePlatformLink.rejected, (state) => {
      state.userData.isLoading = false;
    });
    builder.addCase(blockUser.pending, (state) => {
      state.userData.isLoading = true;
    });
    builder.addCase(blockUser.fulfilled, (state) => {
      state.userData.isLoading = false;
    });
    builder.addCase(blockUser.rejected, (state) => {
      state.userData.isLoading = false;
    });
    builder.addCase(unblockUser.pending, (state) => {
      state.userData.isLoading = true;
    });
    builder.addCase(unblockUser.fulfilled, (state) => {
      state.userData.isLoading = false;
    });
    builder.addCase(deleteUser.pending, (state) => {
      state.userData.isLoading = true;
    });
    builder.addCase(deleteUser.fulfilled, (state, { payload: { userId } }: { payload: { userId: string } }) => {
      state.userData.isLoading = false;
      if (state.userData.list[userId]) {
        delete state.userData.list[userId];
        delete state.userData.search_msisdn;
        delete state.userData.search_email;
      }
    });
    builder.addCase(getOperationAudit.pending, (state) => {
      state.operationAuditData = {
        isLoading: true,
      };
    });
    builder.addCase(getOperationAudit.fulfilled, (state, { payload }) => {
      state.operationAuditData = {
        ...payload,
        isLoading: false,
      };
    });
    builder.addCase(getOperationAudit.rejected, (state, { payload }) => {
      state.operationAuditData = {
        error: <string>payload,
        isLoading: false,
      };
    });

    builder.addCase(registerNewUser.pending, (state) => {
      state.registerForm = {
        isLoading: true,
      };
    });
    builder.addCase(registerNewUser.fulfilled, (state) => {
      state.registerForm = {
        isLoading: false,
      };
    });
    builder.addCase(addPlatformLink.pending, (state) => {
      state.userData.isLoading = true;
    });
    builder.addCase(addPlatformLink.fulfilled, (state, { payload: { platform, userId } }) => {
      state.userData.isLoading = false;
      if (!userId) {
        return;
      }
      const partnerMappingsList = state.userData.list[userId].partnerMappings || [];
      partnerMappingsList[platform.partnerId].push(platform);
    });
    builder.addCase(addPlatformLink.rejected, (state) => {
      state.userData.isLoading = false;
    });
  },
});

export const {
  reducer,
  actions: { changeLoading, changeBlockedToPicker, ressetUserState, setSearchParams },
} = userSlice;

export default reducer;
