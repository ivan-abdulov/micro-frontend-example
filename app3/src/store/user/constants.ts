import { IPlatforms } from './interfaces';

export const platforms: IPlatforms = {
  left: [
    {
      name: 'Контроль кадров',
      code: 'lk_kk',
      type: 'phone',
    },
    {
      name: 'Контроль кадров 2.0',
      code: 'lk_kk2',
      type: 'phone',
    },
    {
      name: '8800',
      code: 'lk_8800',
      type: 'phone',
    },
    {
      name: 'ВАТС',
      code: 'lk_vats',
      type: 'email',
    },
  ],
  right: [
    {
      name: 'МультиФон',
      code: 'lk_multifon',
      type: 'phone',
    },
    {
      name: 'Таргет',
      code: 'lk_target',
      type: 'phone',
    },
  ],
};
