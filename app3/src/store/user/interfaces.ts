import moment from 'moment';

import { IGetUserData, TGetDictionaries, TOperationAuditRespContent } from '@utils/api/user/interfaces';

export interface ILoadUserDataParams {
  search_email?: string;
  search_msisdn?: string;
  search_platform?: string;
  search_platform_value?: string;
}
export interface IUserData {
  isLoading: boolean;
  list: {
    [key: string]: IGetUserData;
  };
  error?: string;
  page?: number;
  search_email?: string;
  search_end?: string;
  search_msisdn?: string;
  search_platform?: string;
  search_platform_value?: string;
  search_start?: string;
}

interface IOperationAudit {
  isLoading: boolean;
  dictionaries?: TGetDictionaries;
  error?: string;
  operationAudit?: TOperationAuditRespContent[];
  operationAuditPageNumber?: number;

  operationAuditPageSize?: number;
  operationAuditTotalElements?: number;
}

export interface IUserState {
  operationAuditData: IOperationAudit;
  registerForm: {
    isLoading: boolean;
  };
  userData: IUserData;
}

export interface Ival {
  created: moment.MomentInput;
  customerId: string;
  externalUserId: string;
  partnerId: string;
  phonenum: string;
}

export interface IaddPlatformLinkParams {
  params: {
    customerId: string;
    externalUserId: string;
    partnerId: string;
  };
  userId?: string;
}

export interface IPlatform {
  code: string;
  name: string;
  type: string;
}
export interface IPlatforms {
  left: IPlatform[];
  right: IPlatform[];
}
