import { combineReducers } from '@reduxjs/toolkit';

import login from './login/slice';
import messages from './messages/slice';
import user from './user/slice';

const rootReducer = combineReducers({ user, messages, login });

export default rootReducer;
