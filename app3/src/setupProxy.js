/* eslint-disable */
const { createProxyMiddleware } = require('http-proxy-middleware');


function onProxyReq(proxyReq) {
  proxyReq.removeHeader('Origin');
}

module.exports = function(app) {
  const backend = 'https://' + process.env.REACT_BACKEND_DOMAIN;
  app.use(
    '**/api/**',
    createProxyMiddleware({
      target: backend,
      changeOrigin: true,
      logLevel: 'debug',
      onProxyReq,
    }),
  );
  app.use(
    '**/sso/**',
    createProxyMiddleware({
      target: backend,
      changeOrigin: true,
      logLevel: 'debug',
      onProxyReq,
    }),
  );
  app.use(
    '**/oauth2-consumer/**',
    createProxyMiddleware({
      target: backend,
      changeOrigin: true,
      logLevel: 'debug',
      onProxyReq,
    }),
  );

};
