import { useMemo } from 'react';

import { REALM_B2B, ROLE_MAP } from '../api/login/constants';
import { useLocalSelector } from './useRedux';

export const useIsB2b = (): boolean => {
  const { currentRealm } = useLocalSelector((state) => state.login);
  return useMemo(() => currentRealm === REALM_B2B, [currentRealm]);
};

type TUseFormattedRoles = {
  title: string | undefined;
  value: string;
}[];
export const useFormattedRealms = (): TUseFormattedRoles => {
  const { orgRoles } = useLocalSelector((state) => state.login);

  return useMemo(
    () =>
      (Object.keys(orgRoles || {}) || []).reduce((acc, realm) => {
        if (ROLE_MAP[realm]) {
          acc.push({
            value: realm,
            title: ROLE_MAP[realm],
          });
        }
        return acc;
      }, [] as TUseFormattedRoles),
    [orgRoles]
  );
};
