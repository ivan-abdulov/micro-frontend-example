import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import { TDispatch, TRootState } from '@app/store';

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useLocalDispatch = (): TDispatch => useDispatch<TDispatch>();
export const useLocalSelector: TypedUseSelectorHook<TRootState> = useSelector;
