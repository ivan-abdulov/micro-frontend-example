import { baseEndPoint } from '@utils/api/constants';

import LoginApi from './login';
import UserApi from './user';

class Api {
  user: UserApi;
  login: LoginApi;

  constructor() {
    this.user = new UserApi({
      baseEndPoint,
    });
    this.login = new LoginApi();
  }
}

export default new Api();
