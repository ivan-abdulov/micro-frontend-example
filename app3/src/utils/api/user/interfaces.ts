import { TRealms } from '../login/interfaces';

export interface IProps {
  baseEndPoint: string;
}

export interface IBlock {
  blockedTo: string;
  blockerId: string;
  code: string;
  fd: string;
  name: string;
}
export interface IUserDataFromServer {
  authErrorsCount: number;
  credentials: {
    login: string;
    passwordExpired: boolean;
  }[];
  id: string;
  msisdn: string;
  networkAuthenticationType: string;
  person: {
    contacts: {
      address: string;
      contactType: string;
    }[];
    displayNameNat: string;
    firstNameNat: string;
    lastNameNat: string;
  };
  registrationIncomplete: boolean;
  blocks?: IBlock[];
}
export interface IUserDataFromByEmailServer {
  authErrorCount: number;
  credentials: {
    login: string;
    passwordExpired: boolean;
  }[];
  id: string;
  msisdn: string;
  networkAuthenticationType: string;
  person: {
    displayNameNat: string;
    firstNameNat: string;
    genericRelations: {
      target: {
        address: string;
        contactType: string;
      };
    }[];
    lastNameNat: string;
  };
  registrationIncomplete: boolean;
  blocks?: {
    blockedAction: string;
    blockedFrom: string;
    blockedTo: string;
    blockerId: string;
    code: string;
    extendedAttributes: any;
    externalId: string;
    fd: string;
    id: string;
    name: string;
    targetId: string;
    targetType: string;
    td: string;
    ts: string;
  }[];
}

export interface IGetUserData {
  blockCode?: string;
  blockedTo?: string;
  blockedToPicker?: number;
  blockerId?: string;
  blockFd?: string;
  blockName?: string;
  blocks?: IBlock[];
  displayFN?: boolean;
  displayName?: string;
  email?: string;
  firstName?: string;
  id?: string;
  impersonate_msisdn?: string;
  isBlocked?: boolean;
  lastName?: string;
  msisdn?: string;
  page?: number;
  partnerMappings?: IGetProductPlatformsResult;
}
export interface IGetUsersListData {
  [msisdn: string]: IGetUserData;
}

export interface IOperationAuditUrlData {
  page: number;
  search_end: string;
  search_start: string;
  search_msisdn?: string;
}

export interface IUrlData {
  realm: TRealms;
  search_email?: string;
  search_msisdn?: string;
  search_platform?: string;
  search_platform_value?: string;
}
export interface IOperationOperationAuditParams {
  page: number;
  search_end: string;
  search_start: string;
  search_msisdn?: string;
}

export interface IBlockUserParams {
  blockedTo: string;
  id: string;
}

export interface IPlatformData {
  auth: {
    clientId: string;
    otpSentCount: number;
    otpTryCount: number;
  };
  created: string;
  customerId: string;
  enabled: boolean;
  externalUser: {
    userId: string;
  };
  externalUserId: string;
  id: string;
  messagePostingAllowed: boolean;
  partnerDataAllowed: boolean;
  partnerId: string;
  realm: string;
  type: string;
  updated: string;
}

export type TPlatformsData = IPlatformData[];

export type TOperationAuditRespContent = {
  authEndpointType: string;
  authLevel: string;
  authType: string;
  clientId: string;
  correlationId: string;
  data: {
    realm: string;
  };
  executionId: string;
  geoIPCityId: string;
  geoIPCityNameNat: string;
  geoIPCountry: string;
  geoIPDatabase: string;
  geoIPDatabaseVersion: string;
  geoIPRegionNameNat: string;
  gotoUrl: string;
  id: string;
  ipAddress: number;
  ipAddressString: string;
  isImpersonated: boolean;
  msisdn: string;
  name: string;
  nodeId: string;
  principalId: string;
  service: string;
  timeEnd: string;
  timeStart: string;
  userAgent: string;
  userAgentBrowserFamily: string;
  userAgentBrowserNameVersion: string;
  userAgentBrowserType: string;
  userAgentDeviceBrand: string;
  userAgentDeviceModel: string;
  userAgentDeviceType: string;
  userAgentOSFamily: string;
  userAgentOSNameVersion: string;
};
export interface IOperationAuditResp {
  content: TOperationAuditRespContent[];
  page: {
    number: number;
    size: number;
    totalElements: number;
    totalPages: number;
  };
}

export interface IDeletePartnerMappingsParams {
  externalUserId: string;
  partnerId: string;
  uid: string;
}

export interface IRegistrationFormValues {
  email: string;
  firstName: string;
  lastName: string;
  msisdn: string;
}

export interface IGetOperationAudit {
  dictionaries: TGetDictionaries;
  operationAudit: TOperationAuditRespContent[];
  operationAuditPageNumber: number;
  operationAuditPageSize: number;
  operationAuditTotalElements: number;

  error?: string;
}

export type TGetDictionaries = {
  k: string;
  v: string;
}[];

export interface IPartnerMapping {
  created: string;
  customerId: string;
  externalUserId: string;
  id: string;
  partnerId: string;
  phonenum: string;
  updated: string;
}
export interface IGetProductPlatformsResult {
  lk_8800: IPartnerMapping[];
  lk_kk: IPartnerMapping[];
  lk_kk2: IPartnerMapping[];
  lk_multifon: IPartnerMapping[];
  lk_target: IPartnerMapping[];
  lk_vats: IPartnerMapping[];
}

export interface IAddPartnerMappingsParams {
  customerId: string;
  externalUserId: string;
  partnerId: string;
  partnerType: string;
}

export interface IMakeSearchUrl {
  search_email?: string;
  search_msisdn?: string;
  search_platform?: string;
  search_platform_value?: string;
}
