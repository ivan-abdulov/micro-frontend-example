import { WRONG_PARAMS } from './constants';
import { IMakeSearchUrl, IUrlData } from './interfaces';

export const formatGetUserDataUrl = (params: IUrlData): string => {
  if (params.search_msisdn) {
    return `/realms/${params.realm}/principals/v2?msisdn=${params.search_msisdn}`;
  }
  if (params.search_email) {
    return `/realms/${params.realm}/principals/v2/by-contact-email/${params.search_email}?historical=false`;
  }
  if (params.search_platform && params.search_platform_value) {
    return `/realms/${params.realm}/principals/v2/by-partner-mapping/?partnerId=${params.search_platform}&externalUserId=${params.search_platform_value}`;
  }
  throw new Error(WRONG_PARAMS);
};

export const makeSearchUrl = (formValues: IMakeSearchUrl): string => {
  if (formValues.search_platform && formValues.search_platform_value) {
    return `/search/platform/${formValues.search_platform}/${formValues.search_platform_value}`;
  }
  if (formValues.search_email) {
    return `/search/email/${formValues.search_email}/`;
  }
  if (formValues.search_msisdn) {
    return `/search/msisdn/${formValues.search_msisdn}/`;
  }
  throw new Error(WRONG_PARAMS);
};
