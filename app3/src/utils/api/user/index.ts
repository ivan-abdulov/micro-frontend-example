import { AxiosResponse } from 'axios';

import { REALM_B2B } from '@app/utils/api/login/constants';
import { HTTP_STATUS_CODE_400, HTTP_STATUS_CODE_404, HTTP_STATUS_CODE_409 } from '@app/utils/axios/constants';
import { formatUserInfoByEmail } from '@app/utils/formatters';
import {
  ADD_PLATFORM_CUSTOMER_ID_PREFIX,
  DICTIONARIES_ERROR,
  PARTNER_MAPPING_ALREADY_EXISTS_ERROR,
  SOMETHING_WENT_WRONG,
  TOO_LARGE_SEARCH_PERIOD,
  TOO_LARGE_SEARCH_PERIOD_CHECK_CODE,
  USER_NOT_FOUND,
  WRONG_PARAMS,
} from '@utils/api/user/constants';
import axiosInstance from '@utils/axios';

import {
  IAddPartnerMappingsParams,
  IDeletePartnerMappingsParams,
  IGetOperationAudit,
  IGetProductPlatformsResult,
  IGetUsersListData,
  IOperationAuditResp,
  IOperationOperationAuditParams,
  IPartnerMapping,
  IPlatformData,
  IProps,
  IRegistrationFormValues,
  IUrlData,
  IUserDataFromByEmailServer,
  IUserDataFromServer,
  TGetDictionaries,
  TPlatformsData,
} from './interfaces';
import { formatGetUserDataUrl } from './utils';

class UserApi {
  private readonly endPoint: string;

  constructor({ baseEndPoint }: IProps) {
    this.endPoint = baseEndPoint;
  }

  async getProductPlatforms(msisdn: string): Promise<IGetProductPlatformsResult> {
    const { data: partnerMappings }: AxiosResponse<TPlatformsData> = await axiosInstance.get(
      `${this.endPoint}/partnerMappings?uid=mid_____${msisdn}`
    );

    const obj = {
      lk_kk: [],
      lk_kk2: [],
      lk_8800: [],
      lk_vats: [],
      lk_target: [],
      lk_multifon: [],
    };

    partnerMappings.forEach(
      (item: {
        created: string;
        customerId: string;
        externalUserId: string;
        id: string;
        partnerId: string;
        updated: string;
      }) => {
        if (typeof obj[item.partnerId] !== 'undefined') {
          obj[item.partnerId].push({
            id: item.id,
            partnerId: item.partnerId,
            externalUserId: item.externalUserId,
            customerId: item.customerId,
            created: item.created,
            updated: item.updated,
            phonenum: item.externalUserId,
          });
        }
      }
    );
    return obj;
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  async getUserData(params: IUrlData): Promise<IGetUsersListData> {
    const resultData: IGetUsersListData = {};

    try {
      const { data }: AxiosResponse<IUserDataFromServer | [IUserDataFromByEmailServer]> = await axiosInstance.get(
        `${this.endPoint}${formatGetUserDataUrl(params)}`
      );
      for await (const userData of Array.isArray(data) ? data.map((item) => formatUserInfoByEmail(item)) : [data]) {
        if (!userData.id) {
          continue;
        }
        let displayNameNat = '',
          firstNameNat = '',
          lastNameNat = '',
          displayFN = false;
        const { id } = userData;
        if (typeof userData.person.displayNameNat !== 'undefined') {
          displayFN = true;
          // eslint-disable-next-line prefer-destructuring
          displayNameNat = userData.person.displayNameNat;
        } // end if

        if (typeof userData.person.firstNameNat !== 'undefined') {
          displayFN = true;
          // eslint-disable-next-line prefer-destructuring
          firstNameNat = userData.person.firstNameNat;
        } // end if
        if (typeof userData.person.lastNameNat !== 'undefined') {
          displayFN = true;
          // eslint-disable-next-line prefer-destructuring
          lastNameNat = userData.person.lastNameNat;
        } // end if

        const person = (userData?.person?.contacts || []).find((p) => p.contactType === 'email');
        const email = person?.address || '';

        resultData[userData.id] = {
          ...resultData[userData.id],
          msisdn: userData.msisdn,
          id,

          displayFN,
          displayName: displayNameNat,
          firstName: firstNameNat,
          lastName: lastNameNat,
          email,
          isBlocked: false,
        };

        if (userData && userData.blocks && userData.blocks.length > 0) {
          const blocks = userData.blocks || [];

          resultData[userData.id] = {
            ...resultData[userData.id],
            isBlocked: true,
            blockerId: blocks[0].blockerId,
            blockName: blocks[0].name || '',
            blockCode: blocks[0].code || '',
            blockFd: blocks[0].fd || '',
            blockedTo: blocks[0].blockedTo || '',
            blocks,
          };
        }
        resultData[userData.id].partnerMappings = await this.getProductPlatforms(userData.msisdn);
      }

      if (!Object.keys(resultData).length) {
        throw new Error(USER_NOT_FOUND);
      }
    } catch (e) {
      if ((e?.status && HTTP_STATUS_CODE_404 === e?.status) || e.message === USER_NOT_FOUND) {
        throw new Error(USER_NOT_FOUND);
      }
      throw new Error(SOMETHING_WENT_WRONG);
    }

    return resultData;
  }

  async getDictionaries(): Promise<TGetDictionaries> {
    try {
      const { data }: AxiosResponse<TGetDictionaries> = await axiosInstance.get(`${this.endPoint}/dictionaries`);
      return data.map(({ k, v }) => ({
        k,
        v,
      }));
    } catch (e) {
      throw new Error(DICTIONARIES_ERROR);
    }
  }

  async getOperationAudit(urlData: IOperationOperationAuditParams): Promise<IGetOperationAudit> {
    if (!urlData.search_msisdn) {
      throw new Error(WRONG_PARAMS);
    }
    try {
      const { data }: AxiosResponse<IOperationAuditResp> = await axiosInstance.get(`${this.endPoint}/operationAudit`, {
        params: {
          fromDate: urlData.search_start,
          toDate: urlData.search_end,
          msisdn: urlData.search_msisdn,
          // on server pages start from 0
          page: urlData.page - 1,
        },
      });
      const dictionaries = await this.getDictionaries();
      return {
        ...urlData,
        operationAudit: data.content,
        operationAuditTotalElements: data.page.totalElements,
        // on server pages start from 0
        // eslint-disable-next-line @typescript-eslint/no-magic-numbers
        operationAuditPageNumber: data.page.number + 1,
        operationAuditPageSize: data.page.size,
        dictionaries,
      };
    } catch (e) {
      if (e?.status && HTTP_STATUS_CODE_404 === e?.status) {
        throw new Error(USER_NOT_FOUND);
      }
      if (
        e?.status &&
        HTTP_STATUS_CODE_400 === e?.status &&
        e?.data?.error?.message?.includes(TOO_LARGE_SEARCH_PERIOD_CHECK_CODE)
      ) {
        throw new Error(TOO_LARGE_SEARCH_PERIOD);
      }
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }

  get2Factor(uid: string | undefined): Promise<AxiosResponse> {
    return axiosInstance.get(`${this.endPoint}/otpSettings?uid=${uid}&parameterName=otp.login.enabled`);
  }

  put2Factor(uid: string | undefined, onFactor: boolean): Promise<AxiosResponse> {
    return axiosInstance.put(
      `${this.endPoint}/otpSettings?uid=${uid}&parameterName=otp.login.enabled`,
      String(onFactor)
    );
  }

  deletePartnerMappings(sendData: IDeletePartnerMappingsParams): Promise<AxiosResponse> {
    return axiosInstance.delete(`${this.endPoint}/partnerMappings`, {
      params: sendData,
    });
  }

  async addPartnerMappings(sendData: IAddPartnerMappingsParams): Promise<IPartnerMapping> {
    try {
      // TODO move to config or somewhere else
      const realm = REALM_B2B;
      const { data: mappingData }: AxiosResponse<IPlatformData> = await axiosInstance.post(
        `${this.endPoint}/realms/${realm}/federation/v2/partnerMappings`,
        {
          ...sendData,
          customerId: `${ADD_PLATFORM_CUSTOMER_ID_PREFIX}${sendData.customerId}`,
        }
      );
      return {
        id: mappingData.id,
        partnerId: mappingData.partnerId,
        externalUserId: mappingData.externalUserId,
        customerId: mappingData.customerId,
        created: mappingData.created,
        updated: mappingData.updated,
        phonenum: mappingData.externalUserId,
      };
    } catch (e) {
      if (HTTP_STATUS_CODE_404 === e?.status) {
        throw new Error(USER_NOT_FOUND);
      } else if (HTTP_STATUS_CODE_409 === e?.status) {
        throw new Error(PARTNER_MAPPING_ALREADY_EXISTS_ERROR);
      }
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }

  sendRegistrationForm(sendData: IRegistrationFormValues): Promise<AxiosResponse> {
    return axiosInstance.post(`${this.endPoint}/principals`, sendData);
  }

  blockUser(id: string, blockedTo: string): Promise<AxiosResponse> {
    // TODO move to config or somewhere else
    const realm = REALM_B2B;
    const blockedAction = 'AUTHENTICATION';
    return axiosInstance.post(
      `${this.endPoint}/realms/${realm}/principals/v2/blocks`,
      { blockedTo, blockedAction },
      {
        params: { id },
      }
    );
  }

  unblockUser(id: string): Promise<AxiosResponse> {
    // TODO move to config or somewhere else
    const realm = REALM_B2B;
    return axiosInstance.delete(`${this.endPoint}/realms/${realm}/principals/v2/blocks`, {
      params: { id },
    });
  }

  deleteUser(msisdn: string): Promise<AxiosResponse> {
    return axiosInstance.delete(`${this.endPoint}/principals`, {
      params: { msisdn },
    });
  }

  getJWT(msisdn: string, realm: string): Promise<AxiosResponse> {
    return axiosInstance.get(`${this.endPoint}/realms/${realm}/jwt/v2`, {
      params: { msisdn },
    });
  }
}

export default UserApi;
