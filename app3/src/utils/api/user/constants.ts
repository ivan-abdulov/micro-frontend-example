export const WRONG_PARAMS = 'Неверные параметры запроса';
export const USER_NOT_FOUND = 'Пользователь не найден';
export const SOMETHING_WENT_WRONG = 'Что-то пошло не так';
export const DICTIONARIES_ERROR = 'Ошибка получения словарей';
export const PARTNER_MAPPING_ALREADY_EXISTS_ERROR = 'Такая привязка уже существует';
export const TOO_LARGE_SEARCH_PERIOD = 'Период поиска слишком большой';

export const ADD_PLATFORM_CUSTOMER_ID_PREFIX = 'mid_____';

export const TOO_LARGE_SEARCH_PERIOD_CHECK_CODE = 'RX_AUDIT______9001';
