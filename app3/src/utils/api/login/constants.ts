export const ROLE_B2B = 'b2b';
export const ROLE_B2C = 'customer';

export const REALM_B2B = 'b2b';
export const REALM_B2C = 'customer';

export const ROLE_MAP = {
  [REALM_B2B]: 'Admin B2B',
  [REALM_B2C]: 'Admin B2C',
};

export const TOKEN_REFRESH_COUNT = 2;

export enum LoginTypes {
  login = 'login',
  loginLocal = 'login-local',
  loginAuto = 'login-auto',
}
