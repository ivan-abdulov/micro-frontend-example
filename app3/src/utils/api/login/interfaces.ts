import { REALM_B2B, REALM_B2C, ROLE_B2B, ROLE_B2C } from './constants';
export interface IProps {
  baseEndPoint: string;
}

export interface ILoginParams {
  login: string;
  password: string;
}

export interface ILoginReqParams {
  client_id: string;
  client_secret: string;
  grant_type: string;
  realm: string;
  response_type: string;
  service: string;
  _eventId?: string;
  execution?: string;
  password?: string;
  skipAutoLogin?: boolean;
  username?: string;
}

interface IConfigurableFilteredSize {
  attributes: {
    max: number;
    min: number;
    skip: string;
  };
  name: 'ConfigurableFilteredSize';
}

interface INotNull {
  name: 'NotNull';
}

interface ISize {
  attributes: {
    max: number;
    min: number;
  };
  name: 'Size';
}

export type TConstrain = IConfigurableFilteredSize | INotNull | ISize;
export type TError = { message: string; field?: string };

export interface ILoginData {
  errors: TError[];
  execution: string;
  form: {
    name: string;
    errors?: any;
    fields?: {
      password: {
        constraints: TConstrain[];
      };
      username: {
        constraints: TConstrain[];
      };
    };
  };
  step: string;
}
export interface ILoginDataSuccess {
  access_token: string;
  expires_in: number;
  JWTToken: string;
  mpt: string;
  mpt_expires_in: number;
  old_token: string;
  refresh_expires_in: number;
  refresh_token: string;
  token_type: string;
  claims?: {
    contactEmail?: string;
    displayName?: string;
  };
}

export interface IGetTokenRequest {
  _eventId?: string;
  execution?: string;
  password?: string;
  username?: string;
}

export type TRoles = typeof ROLE_B2B | typeof ROLE_B2C;
export type TRealms = typeof REALM_B2B | typeof REALM_B2C;
export interface IGetTokenInfoRequest {
  attributes: {
    expires_in: number;
    orgRoles: {
      [key: string]: string[];
    };
    realm: string;
    user_name: string;
  };
  exp: number;
}

export interface ITokenRefreshRequest {
  expires_in: number;
}

export interface ILogoutRequestParams {
  logoutRedirectUrl: string;
}
