import { AxiosResponse } from 'axios';
import qs from 'qs';

import configAdapter from '@app/configs/config';
import { getLoginParams } from '@app/configs/utils';
import { SOMETHING_WENT_WRONG } from '@app/utils/api/user/constants';
import { formatTokenInfo } from '@app/utils/formatters';
import axiosInstance from '@utils/axios';

import { ROLE_B2B } from './constants';
import {
  IGetTokenInfoRequest,
  IGetTokenRequest,
  ILoginData,
  ILoginDataSuccess,
  ILoginReqParams,
  ILogoutRequestParams,
  ITokenRefreshRequest,
} from './interfaces';
import getTokenRequestStub from './stubs/getTokenReq.json';
import getTokenRequestSuccessStub from './stubs/getTokenReqSuccess.json';

class LoginApi {
  async getTokenRequest(params: IGetTokenRequest, useStubs = false): Promise<ILoginData | ILoginDataSuccess> {
    if (useStubs) {
      return new Promise((resolve) => {
        setTimeout(
          () =>
            resolve(
              params.username && params.password
                ? (getTokenRequestSuccessStub as ILoginDataSuccess)
                : (getTokenRequestStub as ILoginData)
            ),
          1000
        );
      });
    }

    const reqParams: ILoginReqParams = {
      ...getLoginParams(),
      grant_type: 'urn:roox:params:oauth:grant-type:m2m',
      service: 'dispatcher',
      response_type: 'cookie',
      ...params,
    };
    const sso_base_url = configAdapter('sso_base_url');

    try {
      const response: AxiosResponse<ILoginData> = await axiosInstance.post(
        `${sso_base_url}/oauth2/access_token`,
        qs.stringify(reqParams),
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        }
      );

      return response.data;
    } catch (e) {
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }

  async getTokenInfoRequest(useStubs = false): Promise<IGetTokenInfoRequest> {
    if (useStubs) {
      return new Promise((resolve) => {
        setTimeout(
          () =>
            resolve({
              exp: Date.now(),
              attributes: {
                orgRoles: {
                  [ROLE_B2B]: ['test'],
                },
                user_name: 'test',
                realm: '/employee',
                expires_in: 997,
              },
            }),
          1000
        );
      });
    }

    const loginParams = getLoginParams();
    const reqParams = {
      client_id: loginParams.client_id,
    };
    const consumer_url = configAdapter('consumer_url');
    try {
      const response: AxiosResponse<IGetTokenInfoRequest> = await axiosInstance.get(`${consumer_url}/tokens/@current`, {
        params: reqParams,
      });

      return formatTokenInfo(response.data);
    } catch (e) {
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }

  async logoutRequest({ logoutRedirectUrl }: ILogoutRequestParams): Promise<AxiosResponse<void>> {
    const reqParams = {
      ...getLoginParams(),
      redirect_uri: logoutRedirectUrl,
    };
    const consumer_url = configAdapter('consumer_url');
    try {
      return axiosInstance.get(`${consumer_url}/logout`, {
        params: reqParams,
      });
    } catch (e) {
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }

  async tokenRefreshRequest(): Promise<AxiosResponse<ITokenRefreshRequest>> {
    const reqParams = {
      ...getLoginParams(),
    };
    const consumer_url = configAdapter('consumer_url');
    try {
      return axiosInstance.post(`${consumer_url}/refresh`, qs.stringify(reqParams));
    } catch (e) {
      throw new Error(SOMETHING_WENT_WRONG);
    }
  }
}

export default LoginApi;
