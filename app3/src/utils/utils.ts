import { REALM_B2B, REALM_B2C, ROLE_B2B, ROLE_B2C } from './api/login/constants';
import { TRealms, TRoles } from './api/login/interfaces';

export const getRealmFromRoles = (roles: TRoles[]): TRealms | '' => {
  switch (true) {
    case roles.includes(ROLE_B2B):
      return REALM_B2B;
    case roles.includes(ROLE_B2C):
      return REALM_B2C;
    default:
      return '';
  }
};
