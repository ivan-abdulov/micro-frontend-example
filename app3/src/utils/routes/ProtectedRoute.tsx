import { ReactElement, useEffect } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router';
import { Redirect, Route } from 'react-router-dom';

import { getTokenInfoRequestThunk } from '@store/login/slice';
import { useLocalDispatch, useLocalSelector } from '@utils/hooks/useRedux';

import { Routes } from './routesMap';

const ProtectedRoute = ({ component: Component, ...props }: RouteProps): ReactElement => {
  const { isAuthenticated } = useLocalSelector((state) => state.login);
  const dispatch = useLocalDispatch();
  useEffect(() => {
    dispatch(getTokenInfoRequestThunk());
  }, [dispatch]);
  const componentRender = (renderProps: RouteComponentProps): ReactElement => {
    if (Component && isAuthenticated) {
      return <Component {...renderProps} />;
    }

    return <Redirect to={{ pathname: Routes.login, state: { from: location } }} />;
  };

  return <Route {...props} render={componentRender} />;
};

export default ProtectedRoute;
