import { ReactElement } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { IRoute } from './interfaces';
import routesMap from './routesMap';

const RouteMap = (): ReactElement => (
  <Switch>
    {routesMap.map(({ path, component, exact }: IRoute) => (
      <Route path={path} component={component} exact={exact} key={path} />
    ))}
    <Redirect exact from="*" to="/" />
  </Switch>
);

export default RouteMap;
