import React from 'react';
import { RouteComponentProps } from 'react-router';

export type TRouteComponent = React.FC<RouteComponentProps<any>>;

export interface IRoute {
  component: TRouteComponent;
  exact: boolean;
  isPrivate: boolean;
  path: string;
}
