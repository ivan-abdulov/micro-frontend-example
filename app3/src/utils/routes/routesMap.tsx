import Logout from '@app/pages/Logout/Logout';
import RegistrationForm from '@components/RegistrationForm/RegistrationForm';
import Historty from '@pages/History/History';
import Home from '@pages/Home/Home';
import Login from '@pages/Login/Login';

export enum Routes {
  home = '/',
  login = '/login',
  loginAuto = '/login-auto',
  loginLocal = '/login-local',
  logout = '/logout',
  registration = '/registration',
  searchMsisdn = '/search/msisdn/:search_msisdn/',
  searchEmail = '/search/email/:search_email/',
  searchPlatform = '/search/platform/:search_platform/:search_platform_value/',
  history = '/history/:search_msisdn/:search_start/:search_end/:page/',
}

const routesMap = [
  {
    path: Routes.home,
    component: Home,
    exact: true,
    isPrivate: true,
  },
  {
    path: Routes.login,
    component: Login,
    exact: true,
    isPrivate: false,
  },
  {
    path: Routes.loginAuto,
    component: Login,
    exact: true,
    isPrivate: false,
  },
  {
    path: Routes.loginLocal,
    component: Login,
    exact: true,
    isPrivate: false,
  },
  {
    path: Routes.logout,
    component: Logout,
    exact: true,
    isPrivate: false,
  },
  {
    path: Routes.registration,
    component: RegistrationForm,
    exact: true,
    isPrivate: true,
  },
  {
    path: Routes.searchMsisdn,
    component: Home,
    exact: true,
    isPrivate: true,
  },
  {
    path: Routes.searchEmail,
    component: Home,
    exact: true,
    isPrivate: true,
  },
  {
    path: Routes.searchPlatform,
    component: Home,
    exact: true,
    isPrivate: true,
  },
  {
    path: Routes.history,
    component: Historty,
    exact: true,
    isPrivate: true,
  },
];

export default routesMap;
