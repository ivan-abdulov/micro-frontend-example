import moment from 'moment';

import { IGetTokenInfoRequest, TConstrain, TError } from '@utils/api/login/interfaces';
import {
  DEFAULT_DATE_INTERVAL,
  ERRORS_TEXTS,
  MSISDN_LENGTH,
  ORG_ROLES_PREFIX,
  URI_DATE_FORMAT,
} from '@utils/constants';
import { IFormErrorsFromServer, IFormValidation } from '@utils/interfaces';

import { IUserDataFromByEmailServer, IUserDataFromServer } from './api/user/interfaces';

export const dateFormatter = (cell?: string): string => {
  if (!cell) {
    return '--';
  }
  const date = moment(cell);
  if (date.format('yyyy') === '9999') {
    return 'Постоянная';
  }
  return moment(cell).format('LLL');
};

export const blockedTimeFormatter = (cell?: string): string => {
  if (!cell) {
    return '--';
  }
  const date = moment(cell);
  if (date.format('yyyy') === '9999') {
    return 'Никогда';
  }
  return moment(cell).fromNow();
};

export const formatSourceName = (source?: string): string => {
  if (source === 'CALL-CENTER') {
    return 'АРМ КЦ';
  }
  return source || '';
};

export const formatConstrains = (constrains?: TConstrain[]): IFormValidation | null => {
  if (!constrains) {
    return null;
  }
  return constrains.reduce((acc: IFormValidation, constrain) => {
    switch (constrain.name) {
      case 'NotNull':
        acc.required = true;
        break;
      case 'ConfigurableFilteredSize':
        acc.pattern = new RegExp(constrain.attributes.skip);
        acc.minLength = constrain.attributes.min;
        acc.maxLength = constrain.attributes.max;
        break;
      case 'Size':
        acc.minLength = constrain.attributes.min;
        acc.maxLength = constrain.attributes.max;
        break;
      default:
    }
    return acc;
  }, {});
};

export const formatErrorsFromServer = (errors?: TError[]): IFormErrorsFromServer | null => {
  if (!errors) {
    return null;
  }
  return errors.reduce((acc: IFormErrorsFromServer, { message, field }): IFormErrorsFromServer => {
    const key = field || 'form';
    const text = ERRORS_TEXTS[message] || message;
    acc[key] = acc[key] ? [acc[key], text].join(', ') : text;
    return acc;
  }, {});
};

export const formatUserInfoByEmail = (info: IUserDataFromByEmailServer): IUserDataFromServer => ({
  id: info?.id,
  msisdn: info?.msisdn,
  person: {
    displayNameNat: info?.person?.displayNameNat,
    firstNameNat: info?.person?.firstNameNat,
    lastNameNat: info?.person?.lastNameNat,
    contacts: (info?.person?.genericRelations || []).map(({ target: { address, contactType } }) => ({
      address,
      contactType,
    })),
  },
  credentials: info.credentials.map(({ login, passwordExpired }: { login: string; passwordExpired: boolean }) => ({
    login,
    passwordExpired,
  })),
  registrationIncomplete: info?.registrationIncomplete,
  authErrorsCount: info?.authErrorCount,
  networkAuthenticationType: info?.networkAuthenticationType,
  blocks: info?.blocks?.map((blockInfo) => ({
    blockedTo: blockInfo.blockedTo,
    blockerId: blockInfo.blockerId,
    fd: blockInfo.fd,
    code: blockInfo.code,
    name: blockInfo.name,
  })),
});

export const formatURLForHistory = (msisdn: string, from?: moment.MomentInput, to?: moment.MomentInput): string => {
  const urlArray = [
    '/history',
    msisdn.substr(-MSISDN_LENGTH), // remove 7 on start of msisdn
    moment(from || moment().subtract(DEFAULT_DATE_INTERVAL, 'd'))
      .utc()
      .format(URI_DATE_FORMAT),
    moment(to).utc().format(URI_DATE_FORMAT),
    '1',
  ];
  return urlArray.join('/');
};

export const phoneFormatter = (value: string, isRemoveFirstNumber = true): string => {
  const formattedPhone = value.replace(/\D/gi, '');
  if (isRemoveFirstNumber) {
    return formattedPhone.substr(-MSISDN_LENGTH);
  }
  return formattedPhone;
};

export const formatTokenInfo = (data: IGetTokenInfoRequest): IGetTokenInfoRequest => {
  console.log('%cformatters.ts line:124 data', 'color: white; background-color: gray;', data);
  return {
    ...data,
    attributes: {
      ...data.attributes,
      orgRoles: Object.entries(data.attributes.orgRoles).reduce((acc, [realm, roles]) => {
        acc[realm.replace(ORG_ROLES_PREFIX, '')] = roles;
        return acc;
      }, {}),
    },
  };
};
