export interface IFormValidation {
  max?: number | string;
  maxLength?: number | string;
  min?: number | string;
  minLength?: number | string;
  pattern?: RegExp;
  required?: boolean;
}

export interface IFormErrorsFromServer {
  [index: string]: string;
}
