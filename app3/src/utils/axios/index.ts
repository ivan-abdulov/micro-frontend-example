import axios, { AxiosInstance } from 'axios';

import { Routes } from '../routes/routesMap';
import { AVAILABLE_STATUS_NUMBERS, HTTP_STATUS_CODE_401 } from './constants';

const [minStatusNumber, maxStatusNumber] = AVAILABLE_STATUS_NUMBERS;

const getHostName = (): string => location.origin;

export const axiosInstance: AxiosInstance = axios.create({
  baseURL: getHostName(),
  headers: {},
  validateStatus: (status: number) => status >= minStatusNumber && status < maxStatusNumber,
  timeout: 60000,
  withCredentials: true,
});

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (
      [HTTP_STATUS_CODE_401].includes(error?.response?.status) &&
      !([Routes.login, Routes.loginLocal, Routes.loginAuto] as string[]).includes(window.location.hash.replace('#', ''))
    ) {
      window.location.hash = `#${Routes.logout}`;
    }
    return Promise.reject(error?.response || error);
  }
);

export const removeDefaultHeaders = (headers: string[]): void => {
  const commonHeaders = { ...axiosInstance.defaults.headers.common };

  axiosInstance.defaults.headers.common = headers.reduce((commonHeadersObj: string, currentHeaderKey: string) => {
    delete commonHeaders[currentHeaderKey];

    return commonHeaders;
  }, commonHeaders);
};

export default axiosInstance;
