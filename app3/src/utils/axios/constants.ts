const successStatus = 200;
const badRequest = 400;

export const HTTP_STATUS_CODE_400 = 400;
export const HTTP_STATUS_CODE_401 = 401;
export const HTTP_STATUS_CODE_403 = 403;
export const HTTP_STATUS_CODE_404 = 404;
export const HTTP_STATUS_CODE_409 = 409;

export const AVAILABLE_STATUS_NUMBERS: number[] = [successStatus, badRequest];
