import { useEffect, useState } from 'react';

import { ILoadComponentProps, ILoadComponentResponse, IScriptProps, IStatus } from './interfaces';

export const loadComponent = ({
  scope,
  module,
}: ILoadComponentProps): (() => Promise<ILoadComponentResponse>) => async (): Promise<ILoadComponentResponse> => {
  // Initializes the share scope. This fills it with known provided modules from this build and all remotes
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  await __webpack_init_sharing__('default');

  const container = window[scope]; // or get the container somewhere else
  // Initialize the container, it may provide shared modules
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  await container.init(__webpack_share_scopes__.default);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const factory = await window[scope].get(module);

  return factory();
};

export const useDynamicScript = ({ url }: IScriptProps): IStatus => {
  const [ready, setReady] = useState<boolean>(false);
  const [failed, setFailed] = useState<boolean>(false);

  useEffect(() => {
    if (!url) {
      return;
    }

    const element = document.createElement('script');

    element.src = url;
    element.type = 'text/javascript';
    element.async = true;

    setReady(false);
    setFailed(false);

    element.onload = () => {
      // eslint-disable-next-line no-console
      console.log(`Dynamic Script Loaded: ${url}`);
      setReady(true);
      setFailed(false);
    };

    element.onerror = () => {
      // eslint-disable-next-line no-console
      console.error(`Dynamic Script Error: ${url}`);
      setReady(false);
      setFailed(true);
    };

    document.head.appendChild(element);

    // TODO возможно потенциально не нужно удалять модули из проекта при unmount, будет понятно на конкретных рабочих кейсах
    // return () => {
    //   console.log(`Dynamic Script Removed: ${url}`);
    //   document.head.removeChild(element);
    // };
  }, [url]);

  return {
    ready,
    failed,
  };
};
