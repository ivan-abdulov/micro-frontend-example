import { ComponentType } from 'react';

export interface IScriptProps {
  url?: string;
}

export interface ILoadComponentProps {
  module: string;
  scope: string;
}

export interface IStatus {
  failed: boolean;
  ready: boolean;
}

export interface ILoadComponentResponse {
  default: ComponentType<any>;
}

export interface IRemoteModule {
  config: IRemoteModuleAPI;
  props?: any;
}

export interface IRemoteModuleAPI {
  module: string;
  scope: string;
  url: string;
}
