import DEV_SETTINGS from '@app/app-settings.development.json';
import PROD_SETTINGS from '@app/app-settings.production.json';

export const MODULE_NAME_MAP = {
  SEARCH_MODULE: 'SEARCH_MODULE',
};

export const getRemoteModuleAPI = (
  { NODE_ENV }: typeof process.env,
  moduleName: keyof typeof MODULE_NAME_MAP
): string => {
  if (NODE_ENV === 'development') {
    return DEV_SETTINGS[moduleName];
  }

  return `${location.origin}${PROD_SETTINGS[moduleName]}`;
};

export const SEARCH_MODULE = {
  url: getRemoteModuleAPI(process.env, 'SEARCH_MODULE'),
  scope: 'vtb_search_module',
  module: './Searchmodule',
};
