import { ReactElement, Suspense, lazy, useEffect } from 'react';

import Loader from '@components/Loader/Loader';
import { DEFAULT_ERROR_MESSAGE } from '@store/messages/constants';
import { showError } from '@store/messages/slice';
import { useLocalDispatch } from '@utils/hooks/useRedux';

import { IRemoteModule } from './interfaces';
import { loadComponent, useDynamicScript } from './utils';

// TODO нужно будет обыграть с помощью дизайнеров состояния загрузки и фэйла для микро фронтов
const RemoteModule = ({ config: { module, scope, url }, props }: IRemoteModule): ReactElement | null => {
  const dispatch = useLocalDispatch();
  const { ready, failed } = useDynamicScript({
    url,
  });

  useEffect(() => {
    if (failed && !ready) {
      dispatch(showError({ msg: DEFAULT_ERROR_MESSAGE }));
    }
  }, [dispatch, failed, ready]);

  if (!(module || scope || url)) {
    return <h2>Not system specified</h2>;
  }

  if (!ready && !failed) {
    return <Loader />;
  }

  if (failed && !ready) {
    return null;
  }

  const Component = lazy(loadComponent({ scope, module }));

  return (
    <Suspense fallback={<Loader />}>
      {props && <Component {...props} />}
      {!props && <Component />}
    </Suspense>
  );
};

RemoteModule.defaultProps = {
  config: {
    module: null,
    scope: null,
    url: null,
  },
};

export default RemoteModule;

export { SEARCH_MODULE } from './configData';
