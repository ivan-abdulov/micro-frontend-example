import localConfig from './localConfig';
/**
 * Адаптер конфигураций проекта. Эту конфигурации поставляются средой
 * исполнения, т.е. тем сервером на котором хранится проект. Для среды разработки
 * конфигурации ипортируются из локального json файла.
 */
declare global {
  interface Window {
    roox_config: typeof localConfig;
  }
}
let config: typeof localConfig;

const configAdapter = (key: string): string | undefined => config[key];

const isDev =
  process.env.NODE_ENV === 'development' ||
  process.env.NODE_ENV === 'test' ||
  typeof window.roox_config === 'undefined';

if (isDev) {
  config = localConfig;
} else {
  config = window.roox_config;
}

export const configObj = config;
export default configAdapter;
