import { LoginTypes } from '@app/utils/api/login/constants';
import { Routes } from '@app/utils/routes/routesMap';

import configAdapter from './config';
import { IloginConfig } from './interfaces';

export const getLoginParams = (type?: string): IloginConfig => {
  const loginType =
    type || (sessionStorage.getItem('loginType') === LoginTypes.loginLocal ? LoginTypes.loginLocal : LoginTypes.login);

  return {
    realm: configAdapter(`login_type.${loginType}.realm`),
    client_id: configAdapter(`login_type.${loginType}.client_id`),
    client_secret: configAdapter(`login_type.${loginType}.client_secret`),
  } as IloginConfig;
};

export const getLogoutRedirectHash = (isManual?: boolean): string => {
  let loginType = sessionStorage.getItem('loginType') || LoginTypes.loginAuto;
  if (isManual && loginType === LoginTypes.loginAuto) {
    loginType = LoginTypes.login;
  }
  if (loginType === LoginTypes.loginAuto) {
    return Routes.loginAuto;
  }
  // for local admins
  if (loginType === LoginTypes.loginLocal) {
    return Routes.loginLocal;
  }
  // for all others admins
  return Routes.login;
};
