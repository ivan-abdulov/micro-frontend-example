export interface IloginConfig {
  client_id: string;
  client_secret: string;
  realm: string;
}
