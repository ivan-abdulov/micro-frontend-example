/* eslint-disable */
export default {
  'sso_base_url': '/sso',
  'consumer_url': '/oauth2-consumer',
  'login_type.login.realm': '/employee',
  'login_type.login.client_id': 'arm_cc_employee',
  'login_type.login.client_secret': 'password',
  'login_type.login-local.realm': '/employee-local',
  'login_type.login-local.client_id': 'arm_cc_emp_local',
  'login_type.login-local.client_secret': 'password'
};
