const CracoAlias = require("craco-alias");
module.exports = {
    plugins: [
        {
            plugin: CracoAlias,
            options: {
                source: "tsconfig",
                tsConfigPath: "./tsconfig.paths.json",
                baseUrl: "./",
                // debug: true
            }
        },
        {
            plugin: require("./craco-plugins/module-federation")
        }
    ],
    webpack: {
        configure: (webpackConfig) => {
            const mode = process.env.NODE_ENV || 'development';
            const publicPath =
                mode === 'development'
                ? `//${process.env.HOST}:${process.env.PORT}/`
                : webpackConfig.output.publicPath;

            // console.log(webpackConfig.output);
            webpackConfig.output = {
                ...webpackConfig.output,
                publicPath,
            };
            return webpackConfig;
        },
    },
};
