// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import { Button } from 'app2/Button';
import React from 'react';

const Home: React.FC = () => (
  <div className="bg-gray-900 p-20 h-screen flex justify-center items-start flex-col">
    <h1 className="text-5xl text-white">Host 👋</h1>
    <React.Suspense fallback="loading">
      <Button>Button from module</Button>
    </React.Suspense>
  </div>
);

export default Home;
