import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import Home from '@Pages/Home';

const App: React.FC = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Home} />
    </Switch>
  </Router>
);

export default App;
