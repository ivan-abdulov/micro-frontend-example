import React from 'react';

import { Counter } from '@features/counter/Counter';

const Home: React.FC = () => (
  <div className="bg-gray-900 p-20 h-screen flex justify-center items-start flex-col">
    <h1 className="text-5xl text-white">Host 👋</h1>
    <Counter />
  </div>
);

export default Home;
